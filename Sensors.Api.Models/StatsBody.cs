﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// StatsBody
    /// </summary>
    [DataContract]
    public partial class StatsBody : IEquatable<StatsBody>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StatsBody" /> class.
        /// </summary>
        [JsonConstructor]
        protected StatsBody()
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="StatsBody" /> class.
        /// </summary>
        /// <param name="version">version (required).</param>
        /// <param name="date">date (required).</param>
        /// <param name="scales">scales (required).</param>
        public StatsBody(int? version = default, DateTime? date = default, List<Scale> scales = default)
        {
            // to ensure "version" is required (not null)
            if (version == null)
            {
                throw new InvalidDataException("version is a required property for StatsBody and cannot be null");
            }
            else
            {
                Version = version;
            }
            // to ensure "date" is required (not null)
            if (date == null)
            {
                throw new InvalidDataException("date is a required property for StatsBody and cannot be null");
            }
            else
            {
                Date = date;
            }
            // to ensure "scales" is required (not null)
            if (scales == null)
            {
                throw new InvalidDataException("scales is a required property for StatsBody and cannot be null");
            }
            else
            {
                Scales = scales;
            }
        }

        /// <summary>
        /// Gets or Sets Version
        /// </summary>
        [DataMember(Name = "version", EmitDefaultValue = false)]
        public int? Version { get; set; }

        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        [DataMember(Name = "date", EmitDefaultValue = false)]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or Sets Scales
        /// </summary>
        [DataMember(Name = "scales", EmitDefaultValue = false)]
        public List<Scale> Scales { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(StatsBody)} {{");
            sb.AppendLine($"\t{nameof(Version)}: {Version}");
            sb.AppendLine($"\t{nameof(Date)}: {Date}");
            sb.AppendLine($"\t{nameof(Scales)}: {Scales}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((StatsBody)input);
        }

        /// <summary>
        /// Returns true if StatsBody instances are equal
        /// </summary>
        /// <param name="input">Instance of StatsBody to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(StatsBody input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Version.IsEqual(input.Version) &&
                   Date.IsEqual(input.Date) &&
                   Scales.IsEqual(input.Scales);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Version != null)
                    hashCode = hashCode * 59 + Version.GetHashCode();
                if (Date != null)
                    hashCode = hashCode * 59 + Date.GetHashCode();
                if (Scales != null)
                    hashCode = hashCode * 59 + Scales.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            // Version (int?) maximum
            if (Version > 1)
            {
                yield return new ValidationResult("Invalid value for Version, must be a value less than or equal to 1.", new[] { nameof(Version) });
            }

            // Version (int?) minimum
            if (Version < 1)
            {
                yield return new ValidationResult("Invalid value for Version, must be a value greater than or equal to 1.", new[] { nameof(Version) });
            }

            yield break;
        }
    }
}
