﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// AppInfo
    /// </summary>
    [DataContract]
    public partial class AppInfo : IEquatable<AppInfo>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AppInfo" /> class.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="version">version.</param>
        /// <param name="localization">localization.</param>
        public AppInfo(string id = default, string version = default, string localization = default)
        {
            Id = id;
            Version = version;
            Localization = localization;
        }

        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public string Id { get; set; }

        /// <summary>
        /// Gets or Sets Version
        /// </summary>
        [DataMember(Name = "version", EmitDefaultValue = false)]
        public string Version { get; set; }

        /// <summary>
        /// Gets or Sets Localization
        /// </summary>
        [DataMember(Name = "localization", EmitDefaultValue = false)]
        public string Localization { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(AppInfo)} {{");
            sb.AppendLine($"\t{nameof(Id)}: {Id}");
            sb.AppendLine($"\t{nameof(Version)}: {Version}");
            sb.AppendLine($"\t{nameof(Localization)}: {Localization}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((AppInfo)input);
        }

        /// <summary>
        /// Returns true if AppInfo instances are equal
        /// </summary>
        /// <param name="input">Instance of AppInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(AppInfo input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Id.IsEqual(input.Id) &&
                   Version.IsEqual(input.Version) &&
                   Localization.IsEqual(input.Localization);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                if (Version != null)
                    hashCode = hashCode * 59 + Version.GetHashCode();
                if (Localization != null)
                    hashCode = hashCode * 59 + Localization.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
