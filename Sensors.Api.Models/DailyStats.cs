﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// DailyStats
    /// </summary>
    [DataContract]
    public partial class DailyStats : IEquatable<DailyStats>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DailyStats" /> class.
        /// </summary>
        [JsonConstructor]
        protected DailyStats()
        {
        }
        
        /// <summary>
        /// Initializes a new instance of the <see cref="DailyStats" /> class.
        /// </summary>
        /// <param name="day">day (required).</param>
        /// <param name="date">date (required).</param>
        /// <param name="count">count (required).</param>
        /// <param name="average">average (required).</param>
        /// <param name="gain">gain (required).</param>
        /// <param name="uni">uni (required).</param>
        /// <param name="sig">sig (required).</param>
        /// <param name="cv">cv (required).</param>
        public DailyStats(int? day = default, DateTime? date = default, int? count = default, int? average = default, int? gain = default, double? uni = default, int? sig = default, double? cv = default)
        {
            // to ensure "day" is required (not null)
            if (day == null)
            {
                throw new InvalidDataException("day is a required property for DailyStats and cannot be null");
            }
            else
            {
                Day = day;
            }
            // to ensure "date" is required (not null)
            if (date == null)
            {
                throw new InvalidDataException("date is a required property for DailyStats and cannot be null");
            }
            else
            {
                Date = date;
            }
            // to ensure "count" is required (not null)
            if (count == null)
            {
                throw new InvalidDataException("count is a required property for DailyStats and cannot be null");
            }
            else
            {
                Count = count;
            }
            // to ensure "average" is required (not null)
            if (average == null)
            {
                throw new InvalidDataException("average is a required property for DailyStats and cannot be null");
            }
            else
            {
                Average = average;
            }
            // to ensure "gain" is required (not null)
            if (gain == null)
            {
                throw new InvalidDataException("gain is a required property for DailyStats and cannot be null");
            }
            else
            {
                Gain = gain;
            }
            // to ensure "uni" is required (not null)
            if (uni == null)
            {
                throw new InvalidDataException("uni is a required property for DailyStats and cannot be null");
            }
            else
            {
                Uni = uni;
            }
            // to ensure "sig" is required (not null)
            if (sig == null)
            {
                throw new InvalidDataException("sig is a required property for DailyStats and cannot be null");
            }
            else
            {
                Sig = sig;
            }
            // to ensure "cv" is required (not null)
            if (cv == null)
            {
                throw new InvalidDataException("cv is a required property for DailyStats and cannot be null");
            }
            else
            {
                Cv = cv;
            }
        }

        /// <summary>
        /// Gets or Sets Day
        /// </summary>
        [DataMember(Name = "day", EmitDefaultValue = false)]
        public int? Day { get; set; }

        /// <summary>
        /// Gets or Sets Date
        /// </summary>
        [DataMember(Name = "date", EmitDefaultValue = false)]
        public DateTime? Date { get; set; }

        /// <summary>
        /// Gets or Sets Count
        /// </summary>
        [DataMember(Name = "count", EmitDefaultValue = false)]
        public int? Count { get; set; }

        /// <summary>
        /// Gets or Sets Average
        /// </summary>
        [DataMember(Name = "average", EmitDefaultValue = false)]
        public int? Average { get; set; }

        /// <summary>
        /// Gets or Sets Gain
        /// </summary>
        [DataMember(Name = "gain", EmitDefaultValue = false)]
        public int? Gain { get; set; }

        /// <summary>
        /// Gets or Sets Uni
        /// </summary>
        [DataMember(Name = "uni", EmitDefaultValue = false)]
        public double? Uni { get; set; }

        /// <summary>
        /// Gets or Sets Sig
        /// </summary>
        [DataMember(Name = "sig", EmitDefaultValue = false)]
        public int? Sig { get; set; }

        /// <summary>
        /// Gets or Sets Cv
        /// </summary>
        [DataMember(Name = "cv", EmitDefaultValue = false)]
        public double? Cv { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(DailyStats)} {{");
            sb.AppendLine($"\t{nameof(Day)}: {Day}");
            sb.AppendLine($"\t{nameof(Date)}: {Date}");
            sb.AppendLine($"\t{nameof(Count)}: {Count}");
            sb.AppendLine($"\t{nameof(Average)}: {Average}");
            sb.AppendLine($"\t{nameof(Gain)}: {Gain}");
            sb.AppendLine($"\t{nameof(Uni)}: {Uni}");
            sb.AppendLine($"\t{nameof(Sig)}: {Sig}");
            sb.AppendLine($"\t{nameof(Cv)}: {Cv}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((DailyStats)input);
        }

        /// <summary>
        /// Returns true if DailyStats instances are equal
        /// </summary>
        /// <param name="input">Instance of DailyStats to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(DailyStats input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Day.IsEqual(input.Day) &&
                   Date.IsEqual(input.Date) &&
                   Count.IsEqual(input.Count) &&
                   Average.IsEqual(input.Average) &&
                   Gain.IsEqual(input.Gain) &&
                   Uni.IsEqual(input.Uni) &&
                   Sig.IsEqual(input.Sig) &&
                   Cv.IsEqual(input.Cv);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Day != null)
                    hashCode = hashCode * 59 + Day.GetHashCode();
                if (Date != null)
                    hashCode = hashCode * 59 + Date.GetHashCode();
                if (Count != null)
                    hashCode = hashCode * 59 + Count.GetHashCode();
                if (Average != null)
                    hashCode = hashCode * 59 + Average.GetHashCode();
                if (Gain != null)
                    hashCode = hashCode * 59 + Gain.GetHashCode();
                if (Uni != null)
                    hashCode = hashCode * 59 + Uni.GetHashCode();
                if (Sig != null)
                    hashCode = hashCode * 59 + Sig.GetHashCode();
                if (Cv != null)
                    hashCode = hashCode * 59 + Cv.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            // Day (int?) maximum
            if (Day > 999)
            {
                yield return new ValidationResult("Invalid value for Day, must be a value less than or equal to 999.", new[] { "Day" });
            }

            // Day (int?) minimum
            if (Day < 0)
            {
                yield return new ValidationResult("Invalid value for Day, must be a value greater than or equal to 0.", new[] { "Day" });
            }

            // Count (int?) maximum
            if (Count > 9999)
            {
                yield return new ValidationResult("Invalid value for Count, must be a value less than or equal to 9999.", new[] { "Count" });
            }

            // Count (int?) minimum
            if (Count < 0)
            {
                yield return new ValidationResult("Invalid value for Count, must be a value greater than or equal to 0.", new[] { "Count" });
            }

            // Average (int?) maximum
            if (Average > 99999)
            {
                yield return new ValidationResult("Invalid value for Average, must be a value less than or equal to 99999.", new[] { "Average" });
            }

            // Average (int?) minimum
            if (Average < 0)
            {
                yield return new ValidationResult("Invalid value for Average, must be a value greater than or equal to 0.", new[] { "Average" });
            }

            // Gain (int?) maximum
            if (Gain > 99999)
            {
                yield return new ValidationResult("Invalid value for Gain, must be a value less than or equal to 99999.", new[] { "Gain" });
            }

            // Gain (int?) minimum
            if (Gain < -99999)
            {
                yield return new ValidationResult("Invalid value for Gain, must be a value greater than or equal to -99999.", new[] { "Gain" });
            }

            // Uni (double?) maximum
            if (Uni > 100)
            {
                yield return new ValidationResult("Invalid value for Uni, must be a value less than or equal to 100.", new[] { "Uni" });
            }

            // Uni (double?) minimum
            if (Uni < 0)
            {
                yield return new ValidationResult("Invalid value for Uni, must be a value greater than or equal to 0.", new[] { "Uni" });
            }

            // Sig (int?) maximum
            if (Sig > 99999)
            {
                yield return new ValidationResult("Invalid value for Sig, must be a value less than or equal to 99999.", new[] { "Sig" });
            }

            // Sig (int?) minimum
            if (Sig < 0)
            {
                yield return new ValidationResult("Invalid value for Sig, must be a value greater than or equal to 0.", new[] { "Sig" });
            }

            // Cv (double?) maximum
            if (Cv > 100)
            {
                yield return new ValidationResult("Invalid value for Cv, must be a value less than or equal to 100.", new[] { "Cv" });
            }

            // Cv (double?) minimum
            if (Cv < 0)
            {
                yield return new ValidationResult("Invalid value for Cv, must be a value greater than or equal to 0.", new[] { "Cv" });
            }

            yield break;
        }
    }
}
