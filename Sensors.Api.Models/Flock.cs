﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// Flock
    /// </summary>
    [DataContract]
    public partial class Flock : IEquatable<Flock>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Flock" /> class.
        /// </summary>
        [JsonConstructor]
        protected Flock()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Flock" /> class.
        /// </summary>
        /// <param name="males">males.</param>
        /// <param name="females">females (required).</param>
        public Flock(SexStats males = default, SexStats females = default)
        {
            // to ensure "females" is required (not null)
            if (females == null)
            {
                throw new InvalidDataException("females is a required property for Flock and cannot be null");
            }
            else
            {
                Females = females;
            }
            Males = males;
        }

        /// <summary>
        /// Gets or Sets Males
        /// </summary>
        [DataMember(Name = "males", EmitDefaultValue = false)]
        public SexStats Males { get; set; }

        /// <summary>
        /// Gets or Sets Females
        /// </summary>
        [DataMember(Name = "females", EmitDefaultValue = false)]
        public SexStats Females { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(Flock)} {{");
            sb.AppendLine($"\t{nameof(Males)}: {Males}");
            sb.AppendLine($"\t{nameof(Females)}: {Females}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((Flock)input);
        }

        /// <summary>
        /// Returns true if Flock instances are equal
        /// </summary>
        /// <param name="input">Instance of Flock to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Flock input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Males.IsEqual(input.Males) &&
                   Females.IsEqual(input.Females);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Males != null)
                    hashCode = hashCode * 59 + Males.GetHashCode();
                if (Females != null)
                    hashCode = hashCode * 59 + Females.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
