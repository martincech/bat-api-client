﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// SexStats
    /// </summary>
    [DataContract]
    public partial class SexStats : IEquatable<SexStats>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SexStats" /> class.
        /// </summary>
        [JsonConstructor]
        protected SexStats()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SexStats" /> class.
        /// </summary>
        /// <param name="curve">curve.</param>
        /// <param name="stats">stats (required).</param>
        public SexStats(List<CurvePoint> curve = default, List<DailyStats> stats = default)
        {
            // to ensure "stats" is required (not null)
            if (stats == null)
            {
                throw new InvalidDataException("stats is a required property for SexStats and cannot be null");
            }
            else
            {
                Stats = stats;
            }
            Curve = curve;
        }

        /// <summary>
        /// Gets or Sets Curve
        /// </summary>
        [DataMember(Name = "curve", EmitDefaultValue = false)]
        public List<CurvePoint> Curve { get; set; }

        /// <summary>
        /// Gets or Sets Stats
        /// </summary>
        [DataMember(Name = "stats", EmitDefaultValue = false)]
        public List<DailyStats> Stats { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(SexStats)} {{");
            sb.AppendLine($"\t{nameof(Curve)}: {Curve}");
            sb.AppendLine($"\t{nameof(Stats)}: {Stats}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((SexStats)input);
        }

        /// <summary>
        /// Returns true if SexStats instances are equal
        /// </summary>
        /// <param name="input">Instance of SexStats to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(SexStats input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Curve.IsEqual(input.Curve) &&
                   Stats.IsEqual(input.Stats);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Curve != null)
                    hashCode = hashCode * 59 + Curve.GetHashCode();
                if (Stats != null)
                    hashCode = hashCode * 59 + Stats.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
