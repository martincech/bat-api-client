﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// OSInfo
    /// </summary>
    [DataContract]
    public partial class OSInfo : IEquatable<OSInfo>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OSInfo" /> class.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="edition">edition.</param>
        /// <param name="version">version.</param>
        /// <param name="localization">localization.</param>
        public OSInfo(string name = default, string edition = default, string version = default, string localization = default)
        {
            Name = name;
            Edition = edition;
            Version = version;
            Localization = localization;
        }

        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [DataMember(Name = "name", EmitDefaultValue = false)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Edition
        /// </summary>
        [DataMember(Name = "edition", EmitDefaultValue = false)]
        public string Edition { get; set; }

        /// <summary>
        /// Gets or Sets Version
        /// </summary>
        [DataMember(Name = "version", EmitDefaultValue = false)]
        public string Version { get; set; }

        /// <summary>
        /// Gets or Sets Localization
        /// </summary>
        [DataMember(Name = "localization", EmitDefaultValue = false)]
        public string Localization { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(OSInfo)} {{");
            sb.AppendLine($"\t{nameof(Name)}: {Name}");
            sb.AppendLine($"\t{nameof(Edition)}: {Edition}");
            sb.AppendLine($"\t{nameof(Version)}: {Version}");
            sb.AppendLine($"\t{nameof(Localization)}: {Localization}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((OSInfo)input);
        }

        /// <summary>
        /// Returns true if OSInfo instances are equal
        /// </summary>
        /// <param name="input">Instance of OSInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(OSInfo input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Name.IsEqual(input.Name) &&
                   Edition.IsEqual(input.Edition) &&
                   Version.IsEqual(input.Version) &&
                   Localization.IsEqual(input.Localization);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                if (Edition != null)
                    hashCode = hashCode * 59 + Edition.GetHashCode();
                if (Version != null)
                    hashCode = hashCode * 59 + Version.GetHashCode();
                if (Localization != null)
                    hashCode = hashCode * 59 + Localization.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
