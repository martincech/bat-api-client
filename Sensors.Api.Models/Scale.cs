﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// Scale
    /// </summary>
    [DataContract]
    public partial class Scale : IEquatable<Scale>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Scale" /> class.
        /// </summary>
        [JsonConstructor]
        protected Scale()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Scale" /> class.
        /// </summary>
        /// <param name="id">id (required).</param>
        /// <param name="displayName">displayName.</param>
        /// <param name="day">day.</param>
        /// <param name="flock">flock.</param>
        public Scale(Guid? id = default, string displayName = default, int? day = default, Flock flock = default)
        {
            // to ensure "id" is required (not null)
            if (id == null)
            {
                throw new InvalidDataException("id is a required property for Scale and cannot be null");
            }
            else
            {
                Id = id;
            }
            DisplayName = displayName;
            Day = day;
            Flock = flock;
        }

        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name = "id", EmitDefaultValue = false)]
        public Guid? Id { get; set; }

        /// <summary>
        /// Gets or Sets DisplayName
        /// </summary>
        [DataMember(Name = "displayName", EmitDefaultValue = false)]
        public string DisplayName { get; set; }

        /// <summary>
        /// Gets or Sets Day
        /// </summary>
        [DataMember(Name = "day", EmitDefaultValue = false)]
        public int? Day { get; set; }

        /// <summary>
        /// Gets or Sets Flock
        /// </summary>
        [DataMember(Name = "flock", EmitDefaultValue = false)]
        public Flock Flock { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(Scale)} {{");
            sb.AppendLine($"\t{nameof(Id)}: {Id}");
            sb.AppendLine($"\t{nameof(DisplayName)}: {DisplayName}");
            sb.AppendLine($"\t{nameof(Day)}: {Day}");
            sb.AppendLine($"\t{nameof(Flock)}: {Flock}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((Scale)input);
        }

        /// <summary>
        /// Returns true if Scale instances are equal
        /// </summary>
        /// <param name="input">Instance of Scale to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Scale input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Id.IsEqual(input.Id) &&
                   DisplayName.IsEqual(input.DisplayName) &&
                   Day.IsEqual(input.Day) &&
                   Flock.IsEqual(input.Flock);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                if (DisplayName != null)
                    hashCode = hashCode * 59 + DisplayName.GetHashCode();
                if (Day != null)
                    hashCode = hashCode * 59 + Day.GetHashCode();
                if (Flock != null)
                    hashCode = hashCode * 59 + Flock.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            // DisplayName (string) maxLength
            if (DisplayName != null && DisplayName.Length > 15)
            {
                yield return new ValidationResult("Invalid value for DisplayName, length must be less than 15.", new[] { nameof(DisplayName) });
            }

            // DisplayName (string) pattern
            var regexDisplayName = new Regex(@"^[0-9\\s]{0,15}$", RegexOptions.CultureInvariant);
            if (false == regexDisplayName.Match(DisplayName).Success)
            {
                yield return new ValidationResult("Invalid value for DisplayName, must match a pattern of " + regexDisplayName, new[] { nameof(DisplayName) });
            }

            // Day (int?) maximum
            if (Day > 999)
            {
                yield return new ValidationResult("Invalid value for Day, must be a value less than or equal to 999.", new[] { nameof(Day) });
            }

            // Day (int?) minimum
            if (Day < 0)
            {
                yield return new ValidationResult("Invalid value for Day, must be a value greater than or equal to 0.", new[] { nameof(Day) });
            }

            yield break;
        }
    }
}
