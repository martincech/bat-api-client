﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// UserInfo
    /// </summary>
    [DataContract]
    public partial class UserInfo : IEquatable<UserInfo>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserInfo" /> class.
        /// </summary>
        /// <param name="os">os.</param>
        /// <param name="app">app.</param>
        public UserInfo(OSInfo os = default, AppInfo app = default)
        {
            Os = os;
            App = app;
        }

        /// <summary>
        /// Gets or Sets Os
        /// </summary>
        [DataMember(Name = "os", EmitDefaultValue = false)]
        public OSInfo Os { get; set; }

        /// <summary>
        /// Gets or Sets App
        /// </summary>
        [DataMember(Name = "app", EmitDefaultValue = false)]
        public AppInfo App { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(UserInfo)} {{");
            sb.AppendLine($"\t{nameof(Os)}: {Os}");
            sb.AppendLine($"\t{nameof(App)}: {App}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((UserInfo)input);
        }

        /// <summary>
        /// Returns true if UserInfo instances are equal
        /// </summary>
        /// <param name="input">Instance of UserInfo to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(UserInfo input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Os.IsEqual(input.Os) &&
                   App.IsEqual(input.App);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Os != null)
                    hashCode = hashCode * 59 + Os.GetHashCode();
                if (App != null)
                    hashCode = hashCode * 59 + App.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }
}
