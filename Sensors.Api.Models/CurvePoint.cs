﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Sensors.Api.Models
{
    /// <summary>
    /// CurvePoint
    /// </summary>
    [DataContract]
    public partial class CurvePoint : IEquatable<CurvePoint>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurvePoint" /> class.
        /// </summary>
        [JsonConstructor]
        protected CurvePoint()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CurvePoint" /> class.
        /// </summary>
        /// <param name="day">day (required).</param>
        /// <param name="weight">weight (required).</param>
        public CurvePoint(int? day = default, int? weight = default)
        {
            // to ensure "day" is required (not null)
            if (day == null)
            {
                throw new InvalidDataException("day is a required property for CurvePoint and cannot be null");
            }
            else
            {
                Day = day;
            }
            // to ensure "weight" is required (not null)
            if (weight == null)
            {
                throw new InvalidDataException("weight is a required property for CurvePoint and cannot be null");
            }
            else
            {
                Weight = weight;
            }
        }

        /// <summary>
        /// Gets or Sets Day
        /// </summary>
        [DataMember(Name = "day", EmitDefaultValue = false)]
        public int? Day { get; set; }

        /// <summary>
        /// Gets or Sets Weight
        /// </summary>
        [DataMember(Name = "weight", EmitDefaultValue = false)]
        public int? Weight { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(CurvePoint)} {{");
            sb.AppendLine($"\t{nameof(Day)}: {Day}");
            sb.AppendLine($"\t{nameof(Weight)}: {Weight}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;
            return input.GetType() == GetType() && Equals((CurvePoint)input);
        }

        /// <summary>
        /// Returns true if CurvePoint instances are equal
        /// </summary>
        /// <param name="input">Instance of CurvePoint to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(CurvePoint input)
        {
            if (input is null) return false;
            if (ReferenceEquals(this, input)) return true;

            return Day.IsEqual(input.Day) &&
                   Weight.IsEqual(input.Weight);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                if (Day != null)
                    hashCode = hashCode * 59 + Day.GetHashCode();
                if (Weight != null)
                    hashCode = hashCode * 59 + Weight.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            // Day (int?) maximum
            if (Day > 999)
            {
                yield return new ValidationResult("Invalid value for Day, must be a value less than or equal to 999.", new[] { "Day" });
            }

            // Day (int?) minimum
            if (Day < 0)
            {
                yield return new ValidationResult("Invalid value for Day, must be a value greater than or equal to 0.", new[] { "Day" });
            }

            // Weight (int?) maximum
            if (Weight > 99999)
            {
                yield return new ValidationResult("Invalid value for Weight, must be a value less than or equal to 99999.", new[] { "Weight" });
            }

            // Weight (int?) minimum
            if (Weight < 0)
            {
                yield return new ValidationResult("Invalid value for Weight, must be a value greater than or equal to 0.", new[] { "Weight" });
            }

            yield break;
        }
    }
}
