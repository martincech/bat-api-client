﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RestSharp;

namespace Api.Common
{
    public abstract class BaseApi
    {
        private ExceptionFactory exceptionFactory = (name, response) => null;
        private const string JsonContentType = "application/json";

        protected static int BadRequestCode => 400;
        protected static int OkCode => 200;

        protected BaseApi(string basePath)
        {
            Configuration = new Configuration { BasePath = basePath };
            ExceptionFactory = Configuration.DefaultExceptionFactory;
        }

        protected BaseApi(Configuration configuration)
        {
            Configuration = configuration ?? Configuration.Default;
            ExceptionFactory = Configuration.DefaultExceptionFactory;
        }


        protected Dictionary<string, string> PathParameters { get; private set; }
        protected List<KeyValuePair<string, string>> QueryParameters { get; private set; }
        protected Dictionary<string, string> HeaderParameters { get; private set; }
        protected Dictionary<string, string> FormParameters { get; private set; }
        protected Dictionary<string, FileParameter> FileParameters { get; private set; }
        protected string HttpContentType { get; set; }


        protected void InitParameters()
        {
            PathParameters = new Dictionary<string, string>();
            QueryParameters = new List<KeyValuePair<string, string>>();
            HeaderParameters = new Dictionary<string, string>(Configuration.DefaultHeader);
            FormParameters = new Dictionary<string, string>();
            FileParameters = new Dictionary<string, FileParameter>();

            // to determine the Content-Type header
            var localVarHttpContentTypes = new string[] { JsonContentType };
            HttpContentType = ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            // to determine the Accept header
            var localVarHttpHeaderAccepts = new string[] { JsonContentType };
            var localVarHttpHeaderAccept = ApiClient.SelectHeaderAccept(localVarHttpHeaderAccepts);
            if (localVarHttpHeaderAccept != null)
            {
                HeaderParameters.Add("Accept", localVarHttpHeaderAccept);
            }
        }

        protected void AddOAuth2Authentication()
        {
            if (string.IsNullOrEmpty(Configuration.AccessToken))
            {
                return;
            }
            HeaderParameters["Authorization"] = "Bearer " + Configuration.AccessToken;
        }

        protected void AddExpandParameter(string expand)
        {
            if (string.IsNullOrEmpty(expand))
            {
                return;
            }
            QueryParameters.AddRange(Configuration.ApiClient.ParameterToKeyValuePairs(string.Empty, nameof(expand), expand));
        }

        /// <summary>
        /// Gets the base path of the API client.
        /// </summary>
        /// <value>The base path</value>
        public string GetBasePath()
        {
            return Configuration.ApiClient.RestClient.BaseUrl.ToString();
        }


        /// <summary>
        /// Gets or sets the configuration object
        /// </summary>
        /// <value>An instance of the Configuration</value>
        public Configuration Configuration { get; set; }

        /// <summary>
        /// Provides a factory method hook for the creation of exceptions.
        /// </summary>
        public ExceptionFactory ExceptionFactory
        {
            get
            {
                if (exceptionFactory != null && exceptionFactory.GetInvocationList().Length > 1)
                {
                    throw new InvalidOperationException("Multicast delegate for ExceptionFactory is unsupported.");
                }
                return exceptionFactory;
            }
            set { exceptionFactory = value; }
        }

        protected async Task<IRestResponse> MakeRequestAsync(string path, Method method, string methodName, object body = null)
        {
            // make the HTTP request
            var localVarResponse = (IRestResponse)await Configuration.ApiClient.CallApiAsync(path,
                method, QueryParameters, body, HeaderParameters, FormParameters, FileParameters,
                PathParameters, HttpContentType);

            if (ExceptionFactory != null)
            {
                var exception = ExceptionFactory(methodName, localVarResponse);
                if (exception != null) throw exception;
            }

            return localVarResponse;
        }
    }
}
