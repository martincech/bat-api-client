﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Administration.Client.Definitions;
using Bat.Api.Client;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Administration.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class CompanyApi : CommonApi, ICompanyApi
    {
        private const string PathCompany = "/company";
        private const string PathCompanyById = "/company/{companyId}";


        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyApi"/> class.
        /// </summary>
        /// <returns></returns>
        public CompanyApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public CompanyApi(Configuration configuration = null)
            : base(configuration)
        {
        }



        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns><see cref="Company"/></returns>
        public Company CompanyGet(Guid companyId)
        {
            var response = CompanyGetWithHttpInfo(companyId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        public ApiResponse<Company> CompanyGetWithHttpInfo(Guid companyId)
        {
            return CompanyGetWithHttpInfoAsync(companyId).Result;
        }

        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns>Task of <see cref="Company"/></returns>
        public async Task<Company> CompanyGetAsync(Guid companyId)
        {
            var response = await CompanyGetWithHttpInfoAsync(companyId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        public async Task<ApiResponse<Company>> CompanyGetWithHttpInfoAsync(Guid companyId)
        {
            // verify the required parameter 'companyId' is set
            if (companyId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, Properties.Resources.MissingRequiredParameter, nameof(companyId), nameof(CompanyApi), nameof(CompanyGet)));

            InitParameters();
            PathParameters.Add(nameof(companyId), Configuration.ApiClient.ParameterToString(companyId)); // path parameter
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathCompanyById, Method.GET, nameof(CompanyGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Company>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Company>(response.Data.ToString()));
        }


        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{Company}"/></returns>
        public IEnumerable<Company> CompanyGet()
        {
            var response = CompanyGetWithHttpInfo();
            return response.Data;
        }

        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{Company}"/></returns>
        public ApiResponse<IEnumerable<Company>> CompanyGetWithHttpInfo()
        {
            return CompanyGetWithHttpInfoAsync().Result;
        }

        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{Company}"/></returns>
        public async Task<IEnumerable<Company>> CompanyGetAsync()
        {
            var response = await CompanyGetWithHttpInfoAsync();
            return response.Data;
        }

        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Company}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Company>>> CompanyGetWithHttpInfoAsync()
        {
            InitParameters();
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathCompany, Method.GET, nameof(CompanyGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Company>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Company>>(response.Data.ToString()));
        }


        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Company"/></returns>
        public Company CompanyPost(CompanyCreate body)
        {
            var response = CompanyPostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        public ApiResponse<Company> CompanyPostWithHttpInfo(CompanyCreate body)
        {
            return CompanyPostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Company"/></returns>
        public async Task<Company> CompanyPostAsync(CompanyCreate body)
        {
            var response = await CompanyPostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        public async Task<ApiResponse<Company>> CompanyPostWithHttpInfoAsync(CompanyCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, Properties.Resources.MissingRequiredParameter, nameof(body), nameof(CompanyApi), nameof(CompanyPost)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathCompany, Method.POST, nameof(CompanyPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Company>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Company>(response.Data.ToString()));
        }


        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Company"/></returns>
        public Company CompanyPut(Guid companyId, Company body)
        {
            var response = CompanyPutWithHttpInfo(companyId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        public ApiResponse<Company> CompanyPutWithHttpInfo(Guid companyId, Company body)
        {
            return CompanyPutWithHttpInfoAsync(companyId, body).Result;
        }

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Company"/></returns>
        public async Task<Company> CompanyPutAsync(Guid companyId, Company body)
        {
            var response = await CompanyPutWithHttpInfoAsync(companyId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        public async Task<ApiResponse<Company>> CompanyPutWithHttpInfoAsync(Guid companyId, Company body)
        {
            // verify the required parameter 'companyId' is set
            if (companyId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, Properties.Resources.MissingRequiredParameter, nameof(companyId), nameof(CompanyApi), nameof(CompanyPut)));
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, Properties.Resources.MissingRequiredParameter, nameof(body), nameof(CompanyApi), nameof(CompanyPut)));

            InitParameters();
            PathParameters.Add(nameof(companyId), Configuration.ApiClient.ParameterToString(companyId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathCompanyById, Method.PUT, nameof(CompanyPut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Company>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Company>(response.Data.ToString()));
        }

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>ApiResponse</returns>
        public bool CompanyDelete(Guid companyId)
        {
            var response = CompanyDeleteWithHttpInfo(companyId);
            return response.Data;
        }

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>ApiResponse of ApiResponse</returns>
        public ApiResponse<bool> CompanyDeleteWithHttpInfo(Guid companyId)
        {
            return CompanyDeleteWithHttpInfoAsync(companyId).Result;
        }

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>Task of ApiResponse</returns>
        public async Task<bool> CompanyDeleteAsync(Guid companyId)
        {
            var response = await CompanyDeleteWithHttpInfoAsync(companyId);
            return response.Data;
        }

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>Task of ApiResponse (ApiResponse)</returns>
        public async Task<ApiResponse<bool>> CompanyDeleteWithHttpInfoAsync(Guid companyId)
        {
            // verify the required parameter 'companyId' is set
            if (companyId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, Properties.Resources.MissingRequiredParameter, nameof(companyId), nameof(CompanyApi), nameof(CompanyDelete)));

            InitParameters();
            PathParameters.Add(nameof(companyId), Configuration.ApiClient.ParameterToString(companyId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathCompanyById, Method.DELETE, nameof(CompanyDelete));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }
    }
}
