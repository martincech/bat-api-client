﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Administration.Client.Definitions
{
    public interface ICompanyApi : IApiAccessor
    {
        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns><see cref="Company"/></returns>
        Company CompanyGet(Guid companyId);

        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        ApiResponse<Company> CompanyGetWithHttpInfo(Guid companyId);

        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns>Task of <see cref="Company"/></returns>
        Task<Company> CompanyGetAsync(Guid companyId);

        /// <summary>
        /// Retrieve Company Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to return</param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        Task<ApiResponse<Company>> CompanyGetWithHttpInfoAsync(Guid companyId);


        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{Company}"/></returns>
        IEnumerable<Company> CompanyGet();

        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{Company}"/></returns>
        ApiResponse<IEnumerable<Company>> CompanyGetWithHttpInfo();

        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{Company}"/></returns>
        Task<IEnumerable<Company>> CompanyGetAsync();

        /// <summary>
        /// List All Companies
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Company}"/>)</returns>
        Task<ApiResponse<IEnumerable<Company>>> CompanyGetWithHttpInfoAsync();


        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Company"/></returns>
        Company CompanyPost(CompanyCreate body);

        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        ApiResponse<Company> CompanyPostWithHttpInfo(CompanyCreate body);

        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Company"/></returns>
        Task<Company> CompanyPostAsync(CompanyCreate body);

        /// <summary>
        /// Create New Company
        /// </summary>
        /// <remarks>
        /// &#39;You may create a company.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        Task<ApiResponse<Company>> CompanyPostWithHttpInfoAsync(CompanyCreate body);

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Company"/></returns>
        Company CompanyPut(Guid companyId, Company body);

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        ApiResponse<Company> CompanyPutWithHttpInfo(Guid companyId, Company body);

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Company"/></returns>
        Task<Company> CompanyPutAsync(Guid companyId, Company body);

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        Task<ApiResponse<Company>> CompanyPutWithHttpInfoAsync(Guid companyId, Company body);

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        bool CompanyDelete(Guid companyId);

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        ApiResponse<bool> CompanyDeleteWithHttpInfo(Guid companyId);

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        Task<bool> CompanyDeleteAsync(Guid companyId);

        /// <summary>
        /// Delete Company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="companyId">UUID v4 ID of Company to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        Task<ApiResponse<bool>> CompanyDeleteWithHttpInfoAsync(Guid companyId);
    }
}
