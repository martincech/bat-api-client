﻿using System;
using System.Diagnostics;
using Xunit;

namespace Sensors.Api.Client.Tests
{
    public class HearbeatApiTests
    {
        private readonly HeartbeatApi instance;

        public HearbeatApiTests()
        {
            instance = new HeartbeatApi();
            instance.Configuration.BasePath = "http://localhost:56889/v1";
            instance.Configuration.AccessToken = "";    //For testing you must add a valid access token
        }

        [Fact(Skip = " ")]
        public void PostHeartbeatTest()
        {
            try
            {
                var response = instance.HeartbeatPostWithHttpInfo(new Models.UserInfo());
            }
            catch (Exception e) //all responses with code >= 400 ends as exception
            {
                Trace.WriteLine(e);
            }
        }
    }
}
