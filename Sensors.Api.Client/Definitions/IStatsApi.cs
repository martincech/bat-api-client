﻿using System.Threading.Tasks;
using Api.Common;
using Sensors.Api.Models;

namespace Sensors.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IStatsApi : IApiAccessor
    {
        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Flag if successfully sent</returns>
        bool StatsPost(StatsBody body);

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of Flag if successfully sent</returns>
        ApiResponse<bool> StatsPostWithHttpInfo(StatsBody body);

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of Flag if successfully sent</returns>
        Task<bool> StatsPostAsync(StatsBody body);

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (Flag if successfully sent)</returns>
        Task<ApiResponse<bool>> StatsPostAsyncWithHttpInfo(StatsBody body);
    }
}
