﻿using System.Threading.Tasks;
using Api.Common;
using Sensors.Api.Models;

namespace Sensors.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IHeartbeatApi : IApiAccessor
    {
        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns></returns>
        object HeartbeatPost(UserInfo info = null);

        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns>ApiResponse of Object(void)</returns>
        ApiResponse<object> HeartbeatPostWithHttpInfo(UserInfo info = null);

        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns>Task of void</returns>
        Task<object> HeartbeatPostAsync(UserInfo info = null);

        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns>Task of ApiResponse</returns>
        Task<ApiResponse<object>> HeartbeatPostAsyncWithHttpInfo(UserInfo info = null);
    }
}
