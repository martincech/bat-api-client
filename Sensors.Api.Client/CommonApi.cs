﻿using Api.Common;

namespace Sensors.Api.Client
{
    public abstract class CommonApi : BaseApi
    {
        static CommonApi()
        {
            Configuration.DefaultAddress = "https://sensorsapi.bat.veit.cz/v1";
            Configuration.Default = new Configuration();
        }

        protected CommonApi(string basePath)
            : base(basePath)
        {
        }

        protected CommonApi(Configuration configuration)
            : base(configuration)
        {
        }
    }
}
