﻿using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;
using RestSharp;
using Sensors.Api.Client.Definitions;
using Sensors.Api.Models;

namespace Sensors.Api.Client
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class StatsApi : CommonApi, IStatsApi
    {
        private const string PathStatistics = "/stats";

        /// <summary>
        /// Initializes a new instance of the <see cref="StatsApi"/> class.
        /// </summary>
        /// <returns></returns>
        public StatsApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatsApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public StatsApi(Configuration configuration = null)
            : base(configuration)
        {
        }

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Flag if successfully sent</returns>
        public bool StatsPost(StatsBody body)
        {
            var response = StatsPostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of Flag if successfully sent</returns>
        public ApiResponse<bool> StatsPostWithHttpInfo(StatsBody body)
        {
            return StatsPostAsyncWithHttpInfo(body).Result;
        }

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of Flag if successfully sent</returns>
        public async Task<bool> StatsPostAsync(StatsBody body)
        {
            var response = await StatsPostAsyncWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Send statistic data
        /// </summary>
        /// <remarks>
        /// Send statistic data
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (Flag if successfully sent)</returns>
        public async Task<ApiResponse<bool>> StatsPostAsyncWithHttpInfo(StatsBody body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
            {
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(StatsApi), nameof(StatsPost)));
            }
                
            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathStatistics, Method.POST, nameof(StatsPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }
    }
}
