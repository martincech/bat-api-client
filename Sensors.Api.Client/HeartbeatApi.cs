﻿using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;
using RestSharp;
using Sensors.Api.Client.Definitions;
using Sensors.Api.Models;

namespace Sensors.Api.Client
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class HeartbeatApi : CommonApi, IHeartbeatApi
    {
        private const string PathHeartbeat = "/heartbeat";

        /// <summary>
        /// Initializes a new instance of the <see cref="HeartbeatApi"/> class.
        /// </summary>
        /// <returns></returns>
        public HeartbeatApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HeartbeatApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public HeartbeatApi(Configuration configuration = null)
            : base(configuration)
        {
        }


        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns></returns>
        public object HeartbeatPost(UserInfo info = null)
        {
            var response = HeartbeatPostWithHttpInfo(info);
            return response.Data;
        }

        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns>ApiResponse of Object(void)</returns>
        public ApiResponse<object> HeartbeatPostWithHttpInfo(UserInfo info = null)
        {
            return HeartbeatPostAsyncWithHttpInfo(info).Result;
        }

        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns>Task of void</returns>
        public async Task<object> HeartbeatPostAsync(UserInfo info = null)
        {
            var response = await HeartbeatPostAsyncWithHttpInfo(info);
            return response.Data;
        }

        /// <summary>
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="info"> (optional)</param>
        /// <returns>Task of ApiResponse</returns>
        public async Task<ApiResponse<object>> HeartbeatPostAsyncWithHttpInfo(UserInfo info = null)
        {
            InitParameters();
            var localVarPostBody = info != null && info.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(info) : info;
            var localVarHttpContentTypes = new string[] 
            {
                "application/json-patch+json",
                "application/json",
                "text/json",
                "application/_*+json"
            };
            HttpContentType = ApiClient.SelectHeaderContentType(localVarHttpContentTypes);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathHeartbeat, Method.POST, nameof(HeartbeatPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<object>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                localVarResponse.Content);
        }
    }
}
