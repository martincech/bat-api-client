﻿using System;
using System.Diagnostics;
using Bat.Api.Client.Api;
using Xunit;

namespace Bat.Api.Client.Test
{
    public class FarmApiTests
    {
        private readonly FarmApi instance;

        public FarmApiTests()
        {
            instance = new FarmApi();
            instance.Configuration.BasePath = "http://localhost:59117/v1";
            instance.Configuration.AccessToken = "";    //For testing you must add a valid access token
        }

        [Fact(Skip = " ")]
        public void FarmGetWithInfoTest()
        {
            try
            {
                var farmId = Guid.Parse("3293E810-B3A2-414D-A501-33FCE8E40F36");
                var response = instance.FarmGetWithHttpInfo(farmId, null);
            }
            catch (Exception e) //all responses with code >= 400 ends as exception
            {
                Trace.WriteLine(e);
            }
        }

        [Fact(Skip = " ")]
        public void FarmGetAllTest()
        {
            var farms = instance.FarmGet(null);
        }

        [Fact(Skip = " ")]
        public void FarmGetAllWithInfoTest()
        {
            var response = instance.FarmGetWithHttpInfo(null);
        }

        [Fact(Skip = " ")]
        public async void FarmGetAllTestAsync()
        {
            var farms = await instance.FarmGetAsync(null);
        }

        [Fact(Skip = " ")]
        public async void FarmGetAllWithInfoTestAsync()
        {
            var response = await instance.FarmGetWithHttpInfoAsync(null);
        }
    }
}
