﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;

namespace Bat.Api.Models
{
    [DataContract]
    public partial class House : HouseCreate, IEquatable<House>
    {
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Required]
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Number of scales assigned to his house
        /// <value>Number of scales assigned to his house</value>
        [DataMember(Name = "scalesCount", EmitDefaultValue = false)]
        public int? ScalesCount { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(House)}: {{");
            sb.AppendLine($"\t{nameof(Id)}: {Id}");
            sb.Append(ToInternalString());
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((House)obj);
        }

        /// <summary>
        /// Returns true if House instances are equal
        /// </summary>
        /// <param name="other">Instance of House to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(House other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id.IsEqual(other.Id) &&
                   Name.IsEqual(other.Name) &&
                   FarmId.IsEqual(other.FarmId);
        }


        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                return base.GetHashCode() * 59 + Id.GetHashCode();
            }
        }

        #region Operators

        public static bool operator ==(House left, House right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(House left, House right)
        {
            return !Equals(left, right);
        }

        #endregion Operators
    }
}
