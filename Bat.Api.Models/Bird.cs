﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;

namespace Bat.Api.Models
{
    [DataContract]
#pragma warning disable CA1724 // Type names should not match namespaces
    public partial class Bird : BirdCreate, IEquatable<Bird>
#pragma warning restore CA1724 // Type names should not match namespaces
    {
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Required]
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(Bird)}: {{");
            sb.AppendLine($"\t{nameof(Id)}: {Id}");
            sb.Append(ToInternalString());
            sb.AppendLine("}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Bird)obj);
        }

        /// <summary>
        /// Returns true if Bird instances are equal
        /// </summary>
        /// <param name="other">Instance of Bird to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Bird other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id.IsEqual(other.Id) &&
                   Company.IsEqual(other.Company) &&
                   Product.IsEqual(other.Product) &&
                   Name.IsEqual(other.Name) &&
                   Sex.IsEqual(other.Sex) &&
                   Duration.IsEqual(other.Duration) &&
                   DateType.IsEqual(other.DateType) &&
                   Scope.IsEqual(other.Scope) &&
                   CurvePoints.IsEqual(other.CurvePoints);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                return base.GetHashCode() * 59 + Id.GetHashCode();
            }
        }

        #region Operators

        public static bool operator ==(Bird left, Bird right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Bird left, Bird right)
        {
            return !Equals(left, right);
        }

        #endregion Operators
    }
}
