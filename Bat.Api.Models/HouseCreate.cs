﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Bat.Api.Models
{
    [DataContract]
    public partial class HouseCreate : IEquatable<HouseCreate>
    {
        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [Required]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets FarmId
        /// </summary>
        [Required]
        [DataMember(Name = "farmId")]
        public Guid FarmId { get; set; }

        /// <summary>
        /// Gets or Sets Farm
        /// </summary>
        [DataMember(Name = "farm", EmitDefaultValue = false)]
        public Farm Farm { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(HouseCreate)} {{");
            sb.Append(ToInternalString());
            sb.AppendLine("}");
            return sb.ToString();
        }

        protected string ToInternalString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"\t{nameof(Name)}: {Name}");
            sb.AppendLine($"\t{nameof(FarmId)}: {FarmId}");
            return sb.ToString();

        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((HouseCreate)obj);
        }

        /// <summary>
        /// Returns true if HouseCreate instances are equal
        /// </summary>
        /// <param name="other">Instance of HouseCreate to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(HouseCreate other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Name.IsEqual(other.Name) &&
                   FarmId.IsEqual(other.FarmId);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                if (FarmId != null)
                    hashCode = hashCode * 59 + FarmId.GetHashCode();
                return hashCode;
            }
        }

        #region Operators

        public static bool operator ==(HouseCreate left, HouseCreate right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(HouseCreate left, HouseCreate right)
        {
            return !Equals(left, right);
        }

        #endregion Operators
    }
}
