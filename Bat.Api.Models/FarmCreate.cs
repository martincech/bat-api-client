﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Bat.Api.Models
{
    [DataContract]
    public partial class FarmCreate : Contact, IEquatable<FarmCreate>
    {
        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [Required]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets Address
        /// </summary>
        [DataMember(Name = "address", EmitDefaultValue = false)]
        public string Address { get; set; }

        /// <summary>
        /// Gets or Sets Country
        /// </summary>
        [DataMember(Name = "country", EmitDefaultValue = false)]
        public string Country { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(FarmCreate)} {{");
            sb.Append(ToInternalString());
            sb.AppendLine("}");
            return sb.ToString();
        }

        protected string ToInternalString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{nameof(Name)}: {Name}");
            sb.AppendLine($"{nameof(Address)}: {Address}");
            sb.AppendLine($"{nameof(Country)}: {Country}");
            sb.AppendLine($"{nameof(ContactName)}: {ContactName}");
            sb.AppendLine($"{nameof(Phone)}: {Phone}");
            sb.AppendLine($"{nameof(Email)}: {Email}");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public override string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((FarmCreate)obj);
        }

        /// <summary>
        /// Returns true if FarmCreate instances are equal
        /// </summary>
        /// <param name="other">Instance of FarmCreate to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(FarmCreate other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Name.IsEqual(other.Name) &&
                   Address.IsEqual(other.Address) &&
                   Country.IsEqual(other.Country) &&
                   ContactName.IsEqual(other.ContactName) &&
                   Phone.IsEqual(other.Phone) &&
                   Email.IsEqual(other.Email);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                if (Address != null)
                    hashCode = hashCode * 59 + Address.GetHashCode();
                if (Country != null)
                    hashCode = hashCode * 59 + Country.GetHashCode();
                if (ContactName != null)
                    hashCode = hashCode * 59 + ContactName.GetHashCode();
                if (Phone != null)
                    hashCode = hashCode * 59 + Phone.GetHashCode();
                if (Email != null)
                    hashCode = hashCode * 59 + Email.GetHashCode();
                return hashCode;
            }
        }

        #region Operators

        public static bool operator ==(FarmCreate left, FarmCreate right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FarmCreate left, FarmCreate right)
        {
            return !Equals(left, right);
        }

        #endregion Operators
    }
}
