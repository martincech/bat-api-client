﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;

namespace Bat.Api.Models
{
    [DataContract]
    public partial class Flock : FlockCreate, IEquatable<Flock>
    {
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [Required]
        [DataMember(Name = "id")]
        public Guid Id { get; set; }


        /// <summary>
        /// Last received statistics
        /// </summary>
        /// <value>Last received statistics</value>
        [DataMember(Name = "lastStatistics")]
        public List<DailyStatistic> LastStatistics { get; set; }


        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(Flock)} {{");
            sb.AppendLine($"{nameof(Id)}: {Id}");
            sb.Append(ToInternalString());
            sb.AppendLine("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Flock)obj);
        }

        /// <summary>
        /// Returns true if Flock instances are equal
        /// </summary>
        /// <param name="other">Instance of Flock to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Flock other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id.IsEqual(other.Id) &&
                   Name.IsEqual(other.Name) &&
                   InitialAge.IsEqual(other.InitialAge) &&
                   InitialWeight.IsEqual(other.InitialWeight) &&
                   TargetAge.IsEqual(other.TargetAge) &&
                   TargetWeight.IsEqual(other.TargetWeight) &&
                   StartDate.IsEqual(other.StartDate) &&
                   EndDate.IsEqual(other.EndDate) &&
                   BirdId.IsEqual(other.BirdId) &&
                   HouseId.IsEqual(other.HouseId) &&
                   DeviceId.IsEqual(other.DeviceId);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                return base.GetHashCode() * 59 + Id.GetHashCode();
            }
        }

        #region Operators

        public static bool operator ==(Flock left, Flock right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Flock left, Flock right)
        {
            return !Equals(left, right);
        }

        #endregion Operators
    }
}
