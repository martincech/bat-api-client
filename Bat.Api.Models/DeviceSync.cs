﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Models.Extensions;
using Newtonsoft.Json;

namespace Bat.Api.Models
{
    [DataContract]
    public partial class DeviceSync : IEquatable<DeviceSync>
    {
        /// <summary>
        /// Device id parameter is optional and will be IGNORED in most cases.
        /// The server auto-assigns UUID of the device with exceptions
        /// of devices implementing properly SMBIOS standard and sending
        /// its UUID assigned by manufacturer. Currently only Terminal and
        /// Gateway device types UUIDs are accepted.
        /// </summary>
        [DataMember(Name = "id")]
        public Guid? Id { get; set; }


        /// <summary>
        /// Gets or Sets Name
        /// </summary>
        [Required]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Assign device to concrete parent
        /// </summary>
        /// <value>Assign device to concrete parent</value>
        [Required]
        [DataMember(Name = "parentId")]
        public Guid ParentId { get; set; }



        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"class {nameof(DeviceSync)} {{");
            sb.AppendLine($"\t{nameof(Id)}: {Id}");
            sb.AppendLine($"\t{nameof(Name)}: {Name}");
            sb.AppendLine($"\t{nameof(ParentId)}: {ParentId}");
            sb.AppendLine("}");
            return sb.ToString();
        }

        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((DeviceSync)obj);
        }

        /// <summary>
        /// Returns true if DeviceSync instances are equal
        /// </summary>
        /// <param name="other">Instance of DeviceSync to be compared</param>
        /// <returns>Boolean</returns>
        public virtual bool Equals(DeviceSync other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return Id.IsEqual(other.Id) &&
                   Name.IsEqual(other.Name) &&
                   ParentId.IsEqual(other.ParentId);
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                var hashCode = 41;
                // Suitable nullity checks etc, of course :)
                if (Id != null)
                    hashCode = hashCode * 59 + Id.GetHashCode();
                if (Name != null)
                    hashCode = hashCode * 59 + Name.GetHashCode();
                if (ParentId != null)
                    hashCode = hashCode * 59 + ParentId.GetHashCode();
                return hashCode;
            }
        }

        #region Operators

        public static bool operator ==(DeviceSync left, DeviceSync right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(DeviceSync left, DeviceSync right)
        {
            return !Equals(left, right);
        }

        #endregion Operators
    }
}
