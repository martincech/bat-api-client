﻿using System.Runtime.Serialization;

namespace Bat.Api.Models.Enums
{
    /// <summary>
    /// Gets or Sets Unit
    /// </summary>
    public enum DeviceUnit
    {
        [EnumMember(Value = "g")]
        G,
        [EnumMember(Value = "kg")]
        Kg,
        [EnumMember(Value = "lb")]
        Lb
    }
}
