﻿using System.Runtime.Serialization;

namespace Bat.Api.Models.Enums
{
    public enum Role
    {
        [EnumMember(Value = "undefined")]
        Undefined = 0,
        [EnumMember(Value = "veitOperator")]
        VeitOperator = 1
    }
}
