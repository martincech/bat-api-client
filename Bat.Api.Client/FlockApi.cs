﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class FlockApi : CommonApi, IFlockApi
    {
        private const string PathFlock = "/flock";
        private const string PathFlockById = "/flock/{farmId}";

        /// <summary>
        /// Initializes a new instance of the <see cref="FlockApi"/> class.
        /// </summary>
        /// <returns></returns>
        public FlockApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FlockApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public FlockApi(Configuration configuration = null)
            : base(configuration)
        {
        }


        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns><see cref="Flock"/></returns>
        public Flock FlockGet(Guid flockId, string expand = null)
        {
            var response = FlockGetWithHttpInfo(flockId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="Flock"/></returns>
        public ApiResponse<Flock> FlockGetWithHttpInfo(Guid flockId, string expand = null)
        {
            return FlockGetWithHttpInfoAsync(flockId, expand).Result;
        }

        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="Flock"/></returns>
        public async Task<Flock> FlockGetAsync(Guid flockId, string expand = null)
        {
            var response = await FlockGetWithHttpInfoAsync(flockId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Flock"/>)</returns>
        public async Task<ApiResponse<Flock>> FlockGetWithHttpInfoAsync(Guid flockId, string expand = null)
        {
            // verify the required parameter 'flockId' is set
            if (flockId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(flockId), nameof(FlockApi), nameof(FlockGet)));

            InitParameters();
            PathParameters.Add(nameof(flockId), Configuration.ApiClient.ParameterToString(flockId));
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFlockById, Method.GET, nameof(FlockGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Flock>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Flock>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns><see cref="IEnumerable{Flock}"/></returns>
        public IEnumerable<Flock> FlockGet(string expand = null)
        {
            var response = FlockGetWithHttpInfo(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Flock}"/></returns>
        public ApiResponse<IEnumerable<Flock>> FlockGetWithHttpInfo(string expand = null)
        {
            return FlockGetWithHttpInfoAsync(expand).Result;
        }

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Flock}"/></returns>
        public async Task<IEnumerable<Flock>> FlockGetAsync(string expand = null)
        {
            var response = await FlockGetWithHttpInfoAsync(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Flock}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Flock>>> FlockGetWithHttpInfoAsync(string expand = null)
        {
            InitParameters();
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFlock, Method.GET, nameof(FlockGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Flock>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Flock>>(response.Data.ToString()));
        }

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Flock"/></returns>
        public Flock FlockPost(FlockCreate body)
        {
            var response = FlockPostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Flock"/></returns>
        public ApiResponse<Flock> FlockPostWithHttpInfo(FlockCreate body)
        {
            return FlockPostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Flock"/></returns>
        public async Task<Flock> FlockPostAsync(FlockCreate body)
        {
            var response = await FlockPostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Flock"/>)</returns>
        public async Task<ApiResponse<Flock>> FlockPostWithHttpInfoAsync(FlockCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(FlockApi), nameof(FlockPost)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFlock, Method.POST, nameof(FlockPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Flock>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Flock>(response.Data.ToString()));
        }

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Flock"/></returns>
        public Flock FlockPut(Guid flockId, Flock body)
        {
            var response = FlockPutWithHttpInfo(flockId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Flock"/></returns>
        public ApiResponse<Flock> FlockPutWithHttpInfo(Guid flockId, Flock body)
        {
            return FlockPutWithHttpInfoAsync(flockId, body).Result;
        }

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Flock"/></returns>
        public async Task<Flock> FlockPutAsync(Guid flockId, Flock body)
        {
            var response = await FlockPutWithHttpInfoAsync(flockId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Flock"/>)</returns>
        public async Task<ApiResponse<Flock>> FlockPutWithHttpInfoAsync(Guid flockId, Flock body)
        {
            // verify the required parameter 'flockId' is set
            if (flockId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(flockId), nameof(FlockApi), nameof(FlockPut)));
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(FlockApi), nameof(FlockPut)));

            InitParameters();
            PathParameters.Add(nameof(flockId), Configuration.ApiClient.ParameterToString(flockId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFlockById, Method.PUT, nameof(FlockPut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Flock>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Flock>(response.Data.ToString()));
        }

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        public bool FlockDelete(Guid flockId)
        {
            var response = FlockDeleteWithHttpInfo(flockId);
            return response.Data;
        }

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        public ApiResponse<bool> FlockDeleteWithHttpInfo(Guid flockId)
        {
            return FlockDeleteWithHttpInfoAsync(flockId).Result;
        }

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        public async Task<bool> FlockDeleteAsync(Guid flockId)
        {
            var response = await FlockDeleteWithHttpInfoAsync(flockId);
            return response.Data;
        }

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        public async Task<ApiResponse<bool>> FlockDeleteWithHttpInfoAsync(Guid flockId)
        {
            // verify the required parameter 'flockId' is set
            if (flockId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(flockId), nameof(FlockApi), nameof(FlockDelete)));

            InitParameters();
            PathParameters.Add(nameof(flockId), Configuration.ApiClient.ParameterToString(flockId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFlockById, Method.DELETE, nameof(FlockDelete));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }
    }
}
