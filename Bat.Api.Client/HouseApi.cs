﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class HouseApi : CommonApi, IHouseApi
    {
        private const string PathHouse = "/house";
        private const string PathHouseById = "/house/{houseId}";

        /// <summary>
        /// Initializes a new instance of the <see cref="HouseApi"/> class.
        /// </summary>
        /// <returns></returns>
        public HouseApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="HouseApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public HouseApi(Configuration configuration = null)
            : base(configuration)
        {
        }

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns><see cref="House"/></returns>
        public House HouseGet(Guid houseId, string expand = null)
        {
            var response = HouseGetWithHttpInfo(houseId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="House"/></returns>
        public ApiResponse<House> HouseGetWithHttpInfo(Guid houseId, string expand = null)
        {
            return HouseGetWithHttpInfoAsync(houseId, expand).Result;
        }

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of <see cref="House"/></returns>
        public async Task<House> HouseGetAsync(Guid houseId, string expand = null)
        {
            var response = await HouseGetWithHttpInfoAsync(houseId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="House"/>)</returns>
        public async Task<ApiResponse<House>> HouseGetWithHttpInfoAsync(Guid houseId, string expand = null)
        {
            // verify the required parameter 'houseId' is set
            if (houseId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(houseId), nameof(HouseApi), nameof(HouseGet)));

            InitParameters();
            PathParameters.Add(nameof(houseId), Configuration.ApiClient.ParameterToString(houseId));
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathHouseById, Method.GET, nameof(HouseGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<House>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<House>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns><see cref="IEnumerable{House}"/></returns>
        public IEnumerable<House> HouseGet(string expand = null)
        {
            var response = HouseGetWithHttpInfo(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{House}"/></returns>
        public ApiResponse<IEnumerable<House>> HouseGetWithHttpInfo(string expand = null)
        {
            return HouseGetWithHttpInfoAsync(expand).Result;
        }

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{House}"/></returns>
        public async Task<IEnumerable<House>> HouseGetAsync(string expand = null)
        {
            var response = await HouseGetWithHttpInfoAsync(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{House}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<House>>> HouseGetWithHttpInfoAsync(string expand = null)
        {
            InitParameters();
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathHouse, Method.GET, nameof(HouseGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<House>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<House>>(response.Data.ToString()));
        }

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="House"/></returns>
        public House HousePost(HouseCreate body)
        {
            var response = HousePostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="House"/></returns>
        public ApiResponse<House> HousePostWithHttpInfo(HouseCreate body)
        {
            return HousePostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="House"/></returns>
        public async Task<House> HousePostAsync(HouseCreate body)
        {
            var response = await HousePostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="House"/>)</returns>
        public async Task<ApiResponse<House>> HousePostWithHttpInfoAsync(HouseCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(HouseApi), nameof(HousePost)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathHouse, Method.POST, nameof(HousePost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<House>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<House>(response.Data.ToString()));
        }

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="House"/></returns>
        public House HousePut(Guid houseId, House body)
        {
            var response = HousePutWithHttpInfo(houseId, body);
            return response.Data;
        }

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="House"/></returns>
        public ApiResponse<House> HousePutWithHttpInfo(Guid houseId, House body)
        {
            return HousePutWithHttpInfoAsync(houseId, body).Result;
        }

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="House"/></returns>
        public async Task<House> HousePutAsync(Guid houseId, House body)
        {
            var response = await HousePutWithHttpInfoAsync(houseId, body);
            return response.Data;
        }

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="House"/>)</returns>
        public async Task<ApiResponse<House>> HousePutWithHttpInfoAsync(Guid houseId, House body)
        {
            // verify the required parameter 'houseId' is set
            if (houseId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(houseId), nameof(HouseApi), nameof(HousePut)));
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(HouseApi), nameof(HousePut)));

            InitParameters();
            PathParameters.Add(nameof(houseId), Configuration.ApiClient.ParameterToString(houseId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathHouseById, Method.PUT, nameof(HousePut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<House>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<House>(response.Data.ToString()));
        }

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        public bool HouseDelete(Guid houseId)
        {
            var response = HouseDeleteWithHttpInfo(houseId);
            return response.Data;
        }

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        public ApiResponse<bool> HouseDeleteWithHttpInfo(Guid houseId)
        {
            return HouseDeleteWithHttpInfoAsync(houseId).Result;
        }

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        public async Task<bool> HouseDeleteAsync(Guid houseId)
        {
            var response = await HouseDeleteWithHttpInfoAsync(houseId);
            return response.Data;
        }

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        public async Task<ApiResponse<bool>> HouseDeleteWithHttpInfoAsync(Guid houseId)
        {
            // verify the required parameter 'houseId' is set
            if (houseId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(houseId), nameof(HouseApi), nameof(HouseDelete)));

            InitParameters();
            PathParameters.Add(nameof(houseId), Configuration.ApiClient.ParameterToString(houseId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathHouseById, Method.DELETE, nameof(HouseDelete));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }
    }
}
