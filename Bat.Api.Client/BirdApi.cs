﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class BirdApi : CommonApi, IBirdApi
    {
        private const string PathBird = "/bird";
        private const string PathBirdById = "/bird/{birdId}";

        /// <summary>
        /// Initializes a new instance of the <see cref="BirdApi"/> class.
        /// </summary>
        /// <returns></returns>
        public BirdApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BirdApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public BirdApi(Configuration configuration = null)
            : base(configuration)
        {
        }


        #region Bird Base

        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns><see cref="Bird"/></returns>
        public Bird BirdGet(Guid birdId, string expand = null)
        {
            var response = BirdGetWithHttpInfo(birdId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>ApiResponse of <see cref="Bird"/></returns>
        public ApiResponse<Bird> BirdGetWithHttpInfo(Guid birdId, string expand = null)
        {
            return BirdGetWithHttpInfoAsync(birdId, expand).Result;
        }

        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of <see cref="Bird"/></returns>
        public async Task<Bird> BirdGetAsync(Guid birdId, string expand = null)
        {
            var response = await BirdGetWithHttpInfoAsync(birdId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Bird"/>)</returns>
        public async Task<ApiResponse<Bird>> BirdGetWithHttpInfoAsync(Guid birdId, string expand = null)
        {
            // verify the required parameter 'birdId' is set
            if (birdId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(birdId), nameof(BirdApi), nameof(BirdGet)));

            InitParameters();
            PathParameters.Add(nameof(birdId), Configuration.ApiClient.ParameterToString(birdId));
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathBirdById, Method.GET, nameof(BirdGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Bird>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Bird>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        public IEnumerable<Bird> BirdGet(string expand = null)
        {
            var response = BirdGetWithHttpInfo(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        public ApiResponse<IEnumerable<Bird>> BirdGetWithHttpInfo(string expand = null)
        {
            return BirdGetWithHttpInfoAsync(expand).Result;
        }

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        public async Task<IEnumerable<Bird>> BirdGetAsync(string expand = null)
        {
            var response = await BirdGetWithHttpInfoAsync(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Bird>>> BirdGetWithHttpInfoAsync(string expand = null)
        {
            InitParameters();
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathBird, Method.GET, nameof(BirdGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Bird>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Bird>>(response.Data.ToString()));
        }

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Bird"/></returns>
        public Bird BirdPost(BirdCreate body)
        {
            var response = BirdPostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Bird"/></returns>
        public ApiResponse<Bird> BirdPostWithHttpInfo(BirdCreate body)
        {
            return BirdPostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Bird"/></returns>
        public async Task<Bird> BirdPostAsync(BirdCreate body)
        {
            var response = await BirdPostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Bird"/>)</returns>
        public async Task<ApiResponse<Bird>> BirdPostWithHttpInfoAsync(BirdCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(BirdApi), nameof(BirdPost)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathBird, Method.POST, nameof(BirdPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Bird>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Bird>(response.Data.ToString()));
        }

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Bird"/></returns>
        public Bird BirdPut(Guid birdId, Bird body)
        {
            var response = BirdPutWithHttpInfo(birdId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Bird"/></returns>
        public ApiResponse<Bird> BirdPutWithHttpInfo(Guid birdId, Bird body)
        {
            return BirdPutWithHttpInfoAsync(birdId, body).Result;
        }

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Bird"/></returns>
        public async Task<Bird> BirdPutAsync(Guid birdId, Bird body)
        {
            var response = await BirdPutWithHttpInfoAsync(birdId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Bird"/>)</returns>
        public async Task<ApiResponse<Bird>> BirdPutWithHttpInfoAsync(Guid birdId, Bird body)
        {
            // verify the required parameter 'birdId' is set
            if (birdId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(birdId), nameof(BirdApi), nameof(BirdPut)));
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(BirdApi), nameof(BirdPut)));

            InitParameters();
            PathParameters.Add(nameof(birdId), Configuration.ApiClient.ParameterToString(birdId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathBirdById, Method.PUT, nameof(BirdPut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Bird>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Bird>(response.Data.ToString()));
        }


        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        public bool BirdDelete(Guid birdId)
        {
            var response = BirdDeleteWithHttpInfo(birdId);
            return response.Data;
        }

        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        public ApiResponse<bool> BirdDeleteWithHttpInfo(Guid birdId)
        {
            return BirdDeleteWithHttpInfoAsync(birdId).Result;
        }

        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        public async Task<bool> BirdDeleteAsync(Guid birdId)
        {
            var response = await BirdDeleteWithHttpInfoAsync(birdId);
            return response.Data;
        }

        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        public async Task<ApiResponse<bool>> BirdDeleteWithHttpInfoAsync(Guid birdId)
        {
            // verify the required parameter 'birdId' is set
            if (birdId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(birdId), nameof(BirdApi), nameof(BirdDelete)));

            InitParameters();
            PathParameters.Add(nameof(birdId), Configuration.ApiClient.ParameterToString(birdId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathBirdById, Method.DELETE, nameof(BirdDelete));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }

        #endregion

        #region Bird Template

        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        public IEnumerable<Bird> BirdTemplateGet(string expand = null)
        {
            var response = BirdTemplateGetWithHttpInfo(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        public ApiResponse<IEnumerable<Bird>> BirdTemplateGetWithHttpInfo(string expand = null)
        {
            return BirdTemplateGetWithHttpInfoAsync(expand).Result;
        }

        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        public async Task<IEnumerable<Bird>> BirdTemplateGetAsync(string expand = null)
        {
            var response = await BirdTemplateGetWithHttpInfoAsync(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Bird>>> BirdTemplateGetWithHttpInfoAsync(string expand = null)
        {
            InitParameters();
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/bird/template", Method.GET, nameof(BirdTemplateGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Bird>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Bird>>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        public IEnumerable<Bird> BirdTemplateGetByProductName(string name)
        {
            var response = BirdTemplateGetByProductNameWithHttpInfo(name);
            return response.Data;
        }

        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        public ApiResponse<IEnumerable<Bird>> BirdTemplateGetByProductNameWithHttpInfo(string name)
        {
            return BirdTemplateGetByProductNameWithHttpInfoAsync(name).Result;
        }

        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        public async Task<IEnumerable<Bird>> BirdTemplateGetByProductNameAsync(string name)
        {
            var response = await BirdTemplateGetByProductNameWithHttpInfoAsync(name);
            return response.Data;
        }

        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Bird>>> BirdTemplateGetByProductNameWithHttpInfoAsync(string name)
        {
            // verify the required parameter 'name' is set
            if (name == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(name), nameof(BirdApi), nameof(BirdTemplateGetByProductName)));

            InitParameters();
            PathParameters.Add(nameof(name), Configuration.ApiClient.ParameterToString(name));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/bird/template/byProduct/{name}", Method.GET, nameof(BirdTemplateGetByProductName));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Bird>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Bird>>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        public IEnumerable<string> BirdTemplateGetCompany()
        {
            var response = BirdTemplateGetCompanyWithHttpInfo();
            return response.Data;
        }

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        public ApiResponse<IEnumerable<string>> BirdTemplateGetCompanyWithHttpInfo()
        {
            return BirdTemplateGetCompanyWithHttpInfoAsync().Result;
        }

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        public async Task<IEnumerable<string>> BirdTemplateGetCompanyAsync()
        {
            var response = await BirdTemplateGetCompanyWithHttpInfoAsync();
            return response.Data;
        }

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<string>>> BirdTemplateGetCompanyWithHttpInfoAsync()
        {
            InitParameters();
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/bird/template/company", Method.GET, nameof(BirdTemplateGetCompany));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<string>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<string>>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        public IEnumerable<string> BirdTemplateGetCompanyName(string name)
        {
            var response = BirdTemplateGetCompanyNameWithHttpInfo(name);
            return response.Data;
        }

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        public ApiResponse<IEnumerable<string>> BirdTemplateGetCompanyNameWithHttpInfo(string name)
        {
            return BirdTemplateGetCompanyNameWithHttpInfoAsync(name).Result;
        }

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        public async Task<IEnumerable<string>> BirdTemplateGetCompanyNameAsync(string name)
        {
            var response = await BirdTemplateGetCompanyNameWithHttpInfoAsync(name);
            return response.Data;
        }

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<string>>> BirdTemplateGetCompanyNameWithHttpInfoAsync(string name)
        {
            // verify the required parameter 'name' is set
            if (name == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(name), nameof(BirdApi), nameof(BirdTemplateGetCompanyName)));

            InitParameters();
            PathParameters.Add(nameof(name), Configuration.ApiClient.ParameterToString(name));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/bird/template/company/{name}", Method.GET, nameof(BirdTemplateGetCompanyName));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<string>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<string>>(response.Data.ToString()));
        }

        #endregion

        #region Bird Specific

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        public IEnumerable<Bird> BirdGetByProductName(string name)
        {
            var response = BirdGetByProductNameWithHttpInfo(name);
            return response.Data;
        }

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        public ApiResponse<IEnumerable<Bird>> BirdGetByProductNameWithHttpInfo(string name)
        {
            return BirdGetByProductNameWithHttpInfoAsync(name).Result;
        }

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        public async Task<IEnumerable<Bird>> BirdGetByProductNameAsync(string name)
        {
            var response = await BirdGetByProductNameWithHttpInfoAsync(name);
            return response.Data;
        }

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Bird>>> BirdGetByProductNameWithHttpInfoAsync(string name)
        {
            // verify the required parameter 'name' is set
            if (name == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(name), nameof(BirdApi), nameof(BirdGetByProductName)));

            InitParameters();
            PathParameters.Add(nameof(name), Configuration.ApiClient.ParameterToString(name));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/bird/byProduct/{name}", Method.GET, nameof(BirdGetByProductName));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Bird>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Bird>>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        public IEnumerable<string> BirdGetCompany()
        {
            var response = BirdGetCompanyWithHttpInfo();
            return response.Data;
        }

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        public ApiResponse<IEnumerable<string>> BirdGetCompanyWithHttpInfo()
        {
            return BirdGetCompanyWithHttpInfoAsync().Result;
        }

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        public async Task<IEnumerable<string>> BirdGetCompanyAsync()
        {
            var response = await BirdGetCompanyWithHttpInfoAsync();
            return response.Data;
        }

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<string>>> BirdGetCompanyWithHttpInfoAsync()
        {
            InitParameters();
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/bird/company", Method.GET, nameof(BirdGetCompany));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<string>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<string>>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        public IEnumerable<string> BirdGetCompanyName(string name)
        {
            var response = BirdGetCompanyNameWithHttpInfo(name);
            return response.Data;
        }

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        public ApiResponse<IEnumerable<string>> BirdGetCompanyNameWithHttpInfo(string name)
        {
            return BirdGetCompanyNameWithHttpInfoAsync(name).Result;
        }

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        public async Task<IEnumerable<string>> BirdGetCompanyNameAsync(string name)
        {
            var response = await BirdGetCompanyNameWithHttpInfoAsync(name);
            return response.Data;
        }

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<string>>> BirdGetCompanyNameWithHttpInfoAsync(string name)
        {
            // verify the required parameter 'name' is set
            if (name == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(name), nameof(BirdApi), nameof(BirdGetCompanyName)));

            InitParameters();
            PathParameters.Add(nameof(name), Configuration.ApiClient.ParameterToString(name));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/bird/company/{name}", Method.GET, nameof(BirdGetCompanyName));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<string>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<string>>(response.Data.ToString()));
        }

        #endregion
    }
}
