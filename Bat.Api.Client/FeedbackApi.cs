﻿using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class FeedbackApi : CommonApi, IFeedbackApi
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeedbackApi"/> class.
        /// </summary>
        /// <returns></returns>
        public FeedbackApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FeedbackApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public FeedbackApi(Configuration configuration = null)
            : base(configuration)
        {
        }

        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>Flag if successfully send</returns>
        public bool FeedbackPost(Stream zipFile)
        {
            var response = FeedbackPostWithHttpInfo(zipFile);
            return response.Data;
        }

        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>ApiResponse of Flag if successfully send</returns>
        public ApiResponse<bool> FeedbackPostWithHttpInfo(Stream zipFile)
        {
            return FeedbackPostWithHttpInfoAsync(zipFile).Result;
        }

        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>Task of Flag if successfully send</returns>
        public async Task<bool> FeedbackPostAsync(Stream zipFile)
        {
            var response = await FeedbackPostWithHttpInfoAsync(zipFile);
            return response.Data;
        }

        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>Task of ApiResponse (Flag if successfully send)</returns>
        public async Task<ApiResponse<bool>> FeedbackPostWithHttpInfoAsync(Stream zipFile)
        {
            // verify the required parameter 'zipFile' is set
            if (zipFile == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(zipFile), nameof(FeedbackApi), nameof(FeedbackPost)));

            var localVarHttpContentTypes = new string[] {
                "multipart/form-data"
            };
            HttpContentType = ApiClient.SelectHeaderContentType(localVarHttpContentTypes);

            InitParameters();
            FileParameters.Add(nameof(zipFile), ApiClient.ParameterToFile(nameof(zipFile), zipFile));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/feedback", Method.POST, nameof(FeedbackPost));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }
    }
}
