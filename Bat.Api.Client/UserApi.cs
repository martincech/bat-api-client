﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class UserApi : CommonApi, IUserApi
    {
        private const string PathUser = "/user";
        private const string PathUserById = "/user/{userId}";
        private const string PathUserCurrent = "/user/current";

        /// <summary>
        /// Initializes a new instance of the <see cref="UserApi"/> class.
        /// </summary>
        /// <returns></returns>
        public UserApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public UserApi(Configuration configuration = null)
            : base(configuration)
        {
        }


        #region User Base Methods

        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns><see cref="User"/></returns>
        public User UserGet(Guid userId)
        {
            var response = UserGetWithHttpInfo(userId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        public ApiResponse<User> UserGetWithHttpInfo(Guid userId)
        {
            return UserGetWithHttpInfoAsync(userId).Result;
        }

        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns>Task of <see cref="User"/></returns>
        public async Task<User> UserGetAsync(Guid userId)
        {
            var response = await UserGetWithHttpInfoAsync(userId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        public async Task<ApiResponse<User>> UserGetWithHttpInfoAsync(Guid userId)
        {
            // verify the required parameter 'userId' is set
            if (userId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(userId), nameof(UserApi), nameof(UserGet)));

            InitParameters();
            PathParameters.Add(nameof(userId), Configuration.ApiClient.ParameterToString(userId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathUserById, Method.GET, nameof(UserGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<User>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<User>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{User}"/></returns>
        public IEnumerable<User> UserGet()
        {
            var response = UserGetWithHttpInfo();
            return response.Data;
        }

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{User}"/></returns>
        public ApiResponse<IEnumerable<User>> UserGetWithHttpInfo()
        {
            return UserGetWithHttpInfoAsync().Result;
        }

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{User}"/></returns>
        public async Task<IEnumerable<User>> UserGetAsync()
        {
            var response = await UserGetWithHttpInfoAsync();
            return response.Data;
        }

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{User}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<User>>> UserGetWithHttpInfoAsync()
        {
            InitParameters();
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathUser, Method.GET, nameof(UserGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<User>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<User>>(response.Data.ToString()));
        }

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        public User UserPost(UserCreate body)
        {
            var response = UserPostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        public ApiResponse<User> UserPostWithHttpInfo(UserCreate body)
        {
            return UserPostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        public async Task<User> UserPostAsync(UserCreate body)
        {
            var response = await UserPostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        public async Task<ApiResponse<User>> UserPostWithHttpInfoAsync(UserCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(UserApi), nameof(UserPost)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathUser, Method.POST, nameof(UserPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<User>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<User>(response.Data.ToString()));
        }

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        public User UserPut(Guid userId, User body)
        {
            var response = UserPutWithHttpInfo(userId, body);
            return response.Data;
        }

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        public ApiResponse<User> UserPutWithHttpInfo(Guid userId, User body)
        {
            return UserPutWithHttpInfoAsync(userId, body).Result;
        }

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        public async Task<User> UserPutAsync(Guid userId, User body)
        {
            var response = await UserPutWithHttpInfoAsync(userId, body);
            return response.Data;
        }

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        public async Task<ApiResponse<User>> UserPutWithHttpInfoAsync(Guid userId, User body)
        {
            // verify the required parameter 'userId' is set
            if (userId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(userId), nameof(UserApi), nameof(UserPut)));
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(UserApi), nameof(UserPut)));

            InitParameters();
            PathParameters.Add(nameof(userId), Configuration.ApiClient.ParameterToString(userId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathUserById, Method.PUT, nameof(UserPut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<User>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<User>(response.Data.ToString()));
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        public bool UserDelete(Guid userId)
        {
            var response = UserDeleteWithHttpInfo(userId);
            return response.Data;
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        public ApiResponse<bool> UserDeleteWithHttpInfo(Guid userId)
        {
            return UserDeleteWithHttpInfoAsync(userId).Result;
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        public async Task<bool> UserDeleteAsync(Guid userId)
        {
            var response = await UserDeleteWithHttpInfoAsync(userId);
            return response.Data;
        }

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        public async Task<ApiResponse<bool>> UserDeleteWithHttpInfoAsync(Guid userId)
        {
            // verify the required parameter 'userId' is set
            if (userId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(userId), nameof(UserApi), nameof(UserDelete)));

            InitParameters();
            PathParameters.Add(nameof(userId), Configuration.ApiClient.ParameterToString(userId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathUserById, Method.DELETE, nameof(UserDelete));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }

        #endregion

        #region User Extended Methods

        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="User"/></returns>
        public User UserCurrentGet()
        {
            var response = UserCurrentGetWithHttpInfo();
            return response.Data;
        }

        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        public ApiResponse<User> UserCurrentGetWithHttpInfo()
        {
            return UserCurrentGetWithHttpInfoAsync().Result;
        }

        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="User"/></returns>
        public async Task<User> UserCurrentGetAsync()
        {
            var response = await UserCurrentGetWithHttpInfoAsync();
            return response.Data;
        }

        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        public async Task<ApiResponse<User>> UserCurrentGetWithHttpInfoAsync()
        {
            InitParameters();
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathUserCurrent, Method.GET, nameof(UserCurrentGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<User>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<User>(response.Data.ToString()));
        }

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        public User UserCurrentPut(User body)
        {
            var response = UserCurrentPutWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        public ApiResponse<User> UserCurrentPutWithHttpInfo(User body)
        {
            return UserCurrentPutWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        public async Task<User> UserCurrentPutAsync(User body)
        {
            var response = await UserCurrentPutWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        public async Task<ApiResponse<User>> UserCurrentPutWithHttpInfoAsync(User body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(UserApi), nameof(UserCurrentPut)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathUserCurrent, Method.PUT, nameof(UserCurrentPut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<User>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<User>(response.Data.ToString()));
        }

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        public User UserRegistrationPost(UserRegister body)
        {
            var response = UserRegistrationPostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        public ApiResponse<User> UserRegistrationPostWithHttpInfo(UserRegister body)
        {
            return UserRegistrationPostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        public async Task<User> UserRegistrationPostAsync(UserRegister body)
        {
            var response = await UserRegistrationPostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        public async Task<ApiResponse<User>> UserRegistrationPostWithHttpInfoAsync(UserRegister body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(UserApi), nameof(UserRegistrationPost)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/user/registration", Method.POST, nameof(UserRegistrationPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<User>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<User>(response.Data.ToString()));
        }

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>ApiResponse</returns>
        public ApiResponse UserResetAccountPut(Guid userId)
        {
            var response = UserResetAccountPutWithHttpInfo(userId);
            return response.Data;
        }

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>ApiResponse of ApiResponse</returns>
        public ApiResponse<ApiResponse> UserResetAccountPutWithHttpInfo(Guid userId)
        {
            return UserResetAccountPutWithHttpInfoAsync(userId).Result;
        }

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>Task of ApiResponse</returns>
        public async Task<ApiResponse> UserResetAccountPutAsync(Guid userId)
        {
            var response = await UserResetAccountPutWithHttpInfoAsync(userId);
            return response.Data;
        }

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>Task of ApiResponse (ApiResponse)</returns>
        public async Task<ApiResponse<ApiResponse>> UserResetAccountPutWithHttpInfoAsync(Guid userId)
        {
            // verify the required parameter 'userId' is set
            if (userId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(userId), nameof(UserApi), nameof(UserResetAccountPut)));

            InitParameters();
            PathParameters.Add(nameof(userId), Configuration.ApiClient.ParameterToString(userId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/user/resetAccount/{userId}", Method.PUT, nameof(UserResetAccountPut));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<ApiResponse>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<ApiResponse>(response.Data.ToString()));
        }

        #endregion
    }
}
