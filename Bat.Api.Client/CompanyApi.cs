﻿using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class CompanyApi : CommonApi, ICompanyApi
    {
        private const string PathCompanyCurrent = "/company/current";

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyApi"/> class.
        /// </summary>
        /// <returns></returns>
        public CompanyApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public CompanyApi(Configuration configuration = null)
            : base(configuration)
        {
        }


        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="Company"/></returns>
        public Company CompanyCurrentGet()
        {
            var response = CompanyCurrentGetWithHttpInfo();
            return response.Data;
        }

        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        public ApiResponse<Company> CompanyCurrentGetWithHttpInfo()
        {
            return CompanyCurrentGetWithHttpInfoAsync().Result;
        }

        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="Company"/></returns>
        public async Task<Company> CompanyCurrentGetAsync()
        {
            var response = await CompanyCurrentGetWithHttpInfoAsync();
            return response.Data;
        }

        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        public async Task<ApiResponse<Company>> CompanyCurrentGetWithHttpInfoAsync()
        {
            InitParameters();
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathCompanyCurrent, Method.GET, nameof(CompanyCurrentGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Company>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Company>(response.Data.ToString()));
        }

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Company"/></returns>
        public Company CompanyCurrentPut(CompanyCreate body)
        {
            var response = CompanyCurrentPutWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        public ApiResponse<Company> CompanyCurrentPutWithHttpInfo(CompanyCreate body)
        {
            return CompanyCurrentPutWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Company"/></returns>
        public async Task<Company> CompanyCurrentPutAsync(CompanyCreate body)
        {
            var response = await CompanyCurrentPutWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        public async Task<ApiResponse<Company>> CompanyCurrentPutWithHttpInfoAsync(CompanyCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
            {
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(CompanyApi), nameof(CompanyCurrentPut)));
            }

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathCompanyCurrent, Method.PUT, nameof(CompanyCurrentPut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Company>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Company>(response.Data.ToString()));
        }
    }
}
