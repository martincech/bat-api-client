﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    public interface IBirdTemplateBaseApi
    {
        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        IEnumerable<Bird> BirdTemplateGet(string expand = null);

        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        ApiResponse<IEnumerable<Bird>> BirdTemplateGetWithHttpInfo(string expand = null);

        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        Task<IEnumerable<Bird>> BirdTemplateGetAsync(string expand = null);

        /// <summary>
        /// List All Birds Templates
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        Task<ApiResponse<IEnumerable<Bird>>> BirdTemplateGetWithHttpInfoAsync(string expand = null);
    }
}
