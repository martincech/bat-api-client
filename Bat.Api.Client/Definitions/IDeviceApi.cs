﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IDeviceApi : IApiAccessor
    {
        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns><see cref="Device"/></returns>
        Device DeviceGet(Guid deviceId, string expand = null);

        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="Device"/></returns>
        ApiResponse<Device> DeviceGetWithHttpInfo(Guid deviceId, string expand = null);

        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="Device"/></returns>
        Task<Device> DeviceGetAsync(Guid deviceId, string expand = null);

        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Device"/>)</returns>
        Task<ApiResponse<Device>> DeviceGetWithHttpInfoAsync(Guid deviceId, string expand = null);

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns><see cref="IEnumerable{Device}"/></returns>
        IEnumerable<Device> DeviceGet(string expand = null);

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Device}"/></returns>
        ApiResponse<IEnumerable<Device>> DeviceGetWithHttpInfo(string expand = null);

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Device}"/></returns>
        Task<IEnumerable<Device>> DeviceGetAsync(string expand = null);

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Device}"/>)</returns>
        Task<ApiResponse<IEnumerable<Device>>> DeviceGetWithHttpInfoAsync(string expand = null);

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Device"/></returns>
        Device DevicePost(DeviceCreate body);

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Device"/></returns>
        ApiResponse<Device> DevicePostWithHttpInfo(DeviceCreate body);

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Device"/></returns>
        Task<Device> DevicePostAsync(DeviceCreate body);

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Device"/>)</returns>
        Task<ApiResponse<Device>> DevicePostWithHttpInfoAsync(DeviceCreate body);

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Device"/></returns>
        Device DevicePut(Guid deviceId, Device body);

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Device"/></returns>
        ApiResponse<Device> DevicePutWithHttpInfo(Guid deviceId, Device body);

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Device"/></returns>
        Task<Device> DevicePutAsync(Guid deviceId, Device body);

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Device"/>)</returns>
        Task<ApiResponse<Device>> DevicePutWithHttpInfoAsync(Guid deviceId, Device body);

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        bool DeviceDelete(Guid deviceId);

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        ApiResponse<bool> DeviceDeleteWithHttpInfo(Guid deviceId);

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        Task<bool> DeviceDeleteAsync(Guid deviceId);

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        Task<ApiResponse<bool>> DeviceDeleteWithHttpInfoAsync(Guid deviceId);


        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="DeviceSync"/></returns>
        DeviceSync DeviceSynchronizePut(Guid deviceId, DeviceSync body);

        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="DeviceSync"/>)</returns>
        ApiResponse<DeviceSync> DeviceSynchronizePutWithHttpInfo(Guid deviceId, DeviceSync body);

        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="DeviceSync"/></returns>
        Task<DeviceSync> DeviceSynchronizePutAsync(Guid deviceId, DeviceSync body);

        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="DeviceSync"/>)</returns>
        Task<ApiResponse<DeviceSync>> DeviceSynchronizePutWithHttpInfoAsync(Guid deviceId, DeviceSync body);
    }
}
