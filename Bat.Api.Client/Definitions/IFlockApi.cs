﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IFlockApi : IApiAccessor
    {
        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns><see cref="Flock"/></returns>
        Flock FlockGet(Guid flockId, string expand = null);

        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="Flock"/></returns>
        ApiResponse<Flock> FlockGetWithHttpInfo(Guid flockId, string expand = null);

        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="Flock"/></returns>
        Task<Flock> FlockGetAsync(Guid flockId, string expand = null);

        /// <summary>
        /// Retrieve Flock Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Flock"/>)</returns>
        Task<ApiResponse<Flock>> FlockGetWithHttpInfoAsync(Guid flockId, string expand = null);

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns><see cref="IEnumerable{Flock}"/></returns>
        IEnumerable<Flock> FlockGet(string expand = null);

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Flock}"/></returns>
        ApiResponse<IEnumerable<Flock>> FlockGetWithHttpInfo(string expand = null);

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Flock}"/></returns>
        Task<IEnumerable<Flock>> FlockGetAsync(string expand = null);

        /// <summary>
        /// List All Flocks
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Flock object. List of available parameters -  expand&#x3D;initialAge,initialWeight,targetAge,targetWeight,endDate,house,device,bird,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Flock}"/>)</returns>
        Task<ApiResponse<IEnumerable<Flock>>> FlockGetWithHttpInfoAsync(string expand = null);

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Flock"/></returns>
        Flock FlockPost(FlockCreate body);

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Flock"/></returns>
        ApiResponse<Flock> FlockPostWithHttpInfo(FlockCreate body);

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Flock"/></returns>
        Task<Flock> FlockPostAsync(FlockCreate body);

        /// <summary>
        /// Create New Flock
        /// </summary>
        /// <remarks>
        /// &#39;You may create a flock.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Flock"/>)</returns>
        Task<ApiResponse<Flock>> FlockPostWithHttpInfoAsync(FlockCreate body);

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Flock"/></returns>
        Flock FlockPut(Guid flockId, Flock body);

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Flock"/></returns>
        ApiResponse<Flock> FlockPutWithHttpInfo(Guid flockId, Flock body);

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Flock"/></returns>
        Task<Flock> FlockPutAsync(Guid flockId, Flock body);

        /// <summary>
        /// Update Flock details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Flock"/>)</returns>
        Task<ApiResponse<Flock>> FlockPutWithHttpInfoAsync(Guid flockId, Flock body);

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        bool FlockDelete(Guid flockId);

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        ApiResponse<bool> FlockDeleteWithHttpInfo(Guid flockId);

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        Task<bool> FlockDeleteAsync(Guid flockId);

        /// <summary>
        /// Delete Flock
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        Task<ApiResponse<bool>> FlockDeleteWithHttpInfoAsync(Guid flockId);
    }
}
