﻿using System;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IUserApi : IUserBaseApi, IApiAccessor
    {
        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="User"/></returns>
        User UserCurrentGet();

        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        ApiResponse<User> UserCurrentGetWithHttpInfo();

        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="User"/></returns>
        Task<User> UserCurrentGetAsync();

        /// <summary>
        /// Retrieve Current Logged User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        Task<ApiResponse<User>> UserCurrentGetWithHttpInfoAsync();

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        User UserCurrentPut(User body);

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        ApiResponse<User> UserCurrentPutWithHttpInfo(User body);

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        Task<User> UserCurrentPutAsync(User body);

        /// <summary>
        /// Update Current Logged User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        Task<ApiResponse<User>> UserCurrentPutWithHttpInfoAsync(User body);

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        User UserRegistrationPost(UserRegister body);

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        ApiResponse<User> UserRegistrationPostWithHttpInfo(UserRegister body);

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        Task<User> UserRegistrationPostAsync(UserRegister body);

        /// <summary>
        /// Register New User
        /// </summary>
        /// <remarks>
        /// &#39;You may register a user. After you finish registration you will  receive an email with initial password.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        Task<ApiResponse<User>> UserRegistrationPostWithHttpInfoAsync(UserRegister body);

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>ApiResponse</returns>
        ApiResponse UserResetAccountPut(Guid userId);

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>ApiResponse of ApiResponse</returns>
        ApiResponse<ApiResponse> UserResetAccountPutWithHttpInfo(Guid userId);

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>Task of ApiResponse</returns>
        Task<ApiResponse> UserResetAccountPutAsync(Guid userId);

        /// <summary>
        /// Reset user password. Send new by email.
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Reset</param>
        /// <returns>Task of ApiResponse (ApiResponse)</returns>
        Task<ApiResponse<ApiResponse>> UserResetAccountPutWithHttpInfoAsync(Guid userId);
    }
}
