﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Basic REST method for Bird
    /// </summary>
    public interface IBirdBaseApi
    {
        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns><see cref="Bird"/></returns>
        Bird BirdGet(Guid birdId, string expand = null);

        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>ApiResponse of <see cref="Bird"/></returns>
        ApiResponse<Bird> BirdGetWithHttpInfo(Guid birdId, string expand = null);

        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of <see cref="Bird"/></returns>
        Task<Bird> BirdGetAsync(Guid birdId, string expand = null);

        /// <summary>
        /// Retrieve Bird Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Bird"/>)</returns>
        Task<ApiResponse<Bird>> BirdGetWithHttpInfoAsync(Guid birdId, string expand = null);

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        IEnumerable<Bird> BirdGet(string expand = null);

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        ApiResponse<IEnumerable<Bird>> BirdGetWithHttpInfo(string expand = null);

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        Task<IEnumerable<Bird>> BirdGetAsync(string expand = null);

        /// <summary>
        /// List All Birds
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Bird object. List of available parameters -  expand&#x3D;sex,duration,dateType,scope,curvePoints (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        Task<ApiResponse<IEnumerable<Bird>>> BirdGetWithHttpInfoAsync(string expand = null);

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Bird"/></returns>
        Bird BirdPost(BirdCreate body);

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Bird"/></returns>
        ApiResponse<Bird> BirdPostWithHttpInfo(BirdCreate body);

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Bird"/></returns>
        Task<Bird> BirdPostAsync(BirdCreate body);

        /// <summary>
        /// Create New Bird
        /// </summary>
        /// <remarks>
        /// &#39;You may create a bird.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Bird"/>)</returns>
        Task<ApiResponse<Bird>> BirdPostWithHttpInfoAsync(BirdCreate body);

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Bird"/></returns>
        Bird BirdPut(Guid birdId, Bird body);

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Bird"/></returns>
        ApiResponse<Bird> BirdPutWithHttpInfo(Guid birdId, Bird body);

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Bird"/></returns>
        Task<Bird> BirdPutAsync(Guid birdId, Bird body);

        /// <summary>
        /// Update Bird details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Bird"/>)</returns>
        Task<ApiResponse<Bird>> BirdPutWithHttpInfoAsync(Guid birdId, Bird body);


        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        bool BirdDelete(Guid birdId);

        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        ApiResponse<bool> BirdDeleteWithHttpInfo(Guid birdId);

        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        Task<bool> BirdDeleteAsync(Guid birdId);

        /// <summary>
        /// Delete Bird
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="birdId">UUID v4 ID of Bird to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        Task<ApiResponse<bool>> BirdDeleteWithHttpInfoAsync(Guid birdId);
    }
}
