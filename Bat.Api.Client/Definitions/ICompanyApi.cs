﻿using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface ICompanyApi : IApiAccessor
    {
        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="Company"/></returns>
        Company CompanyCurrentGet();

        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        ApiResponse<Company> CompanyCurrentGetWithHttpInfo();

        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="Company"/></returns>
        Task<Company> CompanyCurrentGetAsync();

        /// <summary>
        /// Retrieve Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        Task<ApiResponse<Company>> CompanyCurrentGetWithHttpInfoAsync();

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Company"/></returns>
        Company CompanyCurrentPut(CompanyCreate body);

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Company"/></returns>
        ApiResponse<Company> CompanyCurrentPutWithHttpInfo(CompanyCreate body);

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Company"/></returns>
        Task<Company> CompanyCurrentPutAsync(CompanyCreate body);

        /// <summary>
        /// Update Company Info
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Company"/>)</returns>
        Task<ApiResponse<Company>> CompanyCurrentPutWithHttpInfoAsync(CompanyCreate body);
    }
}
