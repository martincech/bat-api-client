﻿using System.IO;
using System.Threading.Tasks;
using Api.Common;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IFeedbackApi : IApiAccessor
    {
        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>Flag if successfully send</returns>
        bool FeedbackPost(Stream zipFile);

        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>ApiResponse of Flag if successfully send</returns>
        ApiResponse<bool> FeedbackPostWithHttpInfo(Stream zipFile);

        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>Task of Flag if successfully send</returns>
        Task<bool> FeedbackPostAsync(Stream zipFile);

        /// <summary>
        /// Send a New Feedback
        /// </summary>
        /// <remarks>
        /// &#39;You may send a feedback.&#39;
        /// </remarks>
        /// <exception cref="IO.Swagger.Client.ApiException">Thrown when fails to make API call</exception>
        /// <param name="zipFile">Zip archive contains text message of feedback.  Archive can contains some diagnostic files.</param>
        /// <returns>Task of ApiResponse (Flag if successfully send)</returns>
        Task<ApiResponse<bool>> FeedbackPostWithHttpInfoAsync(Stream zipFile);
    }
}
