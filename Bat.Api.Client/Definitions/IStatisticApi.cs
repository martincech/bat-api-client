﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IStatisticApi : IApiAccessor
    {
        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        IEnumerable<DailyStatistic> StatisticGetAllByDevice(Guid deviceId);

        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        ApiResponse<IEnumerable<DailyStatistic>> StatisticGetAllByDeviceWithHttpInfo(Guid deviceId);

        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        Task<IEnumerable<DailyStatistic>> StatisticGetAllByDeviceAsync(Guid deviceId);

        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetAllByDeviceWithHttpInfoAsync(Guid deviceId);

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        IEnumerable<DailyStatistic> StatisticGetAllByFlock(Guid flockId);

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        ApiResponse<IEnumerable<DailyStatistic>> StatisticGetAllByFlockWithHttpInfo(Guid flockId);

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        Task<IEnumerable<DailyStatistic>> StatisticGetAllByFlockAsync(Guid flockId);

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetAllByFlockWithHttpInfoAsync(Guid flockId);

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        IEnumerable<DailyStatistic> StatisticGetLastByDevice(List<Guid> deviceIds);

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        ApiResponse<IEnumerable<DailyStatistic>> StatisticGetLastByDeviceWithHttpInfo(List<Guid> deviceIds);

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        Task<IEnumerable<DailyStatistic>> StatisticGetLastByDeviceAsync(List<Guid> deviceIds);

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetLastByDeviceWithHttpInfoAsync(List<Guid> deviceIds);

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        IEnumerable<DailyStatistic> StatisticGetLastByFlock(List<Guid> flockIds);

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        ApiResponse<IEnumerable<DailyStatistic>> StatisticGetLastByFlockWithHttpInfo(List<Guid> flockIds);

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        Task<IEnumerable<DailyStatistic>> StatisticGetLastByFlockAsync(List<Guid> flockIds);

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetLastByFlockWithHttpInfoAsync(List<Guid> flockIds);
    }
}
