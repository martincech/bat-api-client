﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IBirdApi : IApiAccessor, IBirdBaseApi, IBirdTemplateApi
    {
        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        IEnumerable<Bird> BirdGetByProductName(string name);

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        ApiResponse<IEnumerable<Bird>> BirdGetByProductNameWithHttpInfo(string name);

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        Task<IEnumerable<Bird>> BirdGetByProductNameAsync(string name);

        /// <summary>
        /// List All Birds of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird product name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        Task<ApiResponse<IEnumerable<Bird>>> BirdGetByProductNameWithHttpInfoAsync(string name);

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        IEnumerable<string> BirdGetCompany();

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        ApiResponse<IEnumerable<string>> BirdGetCompanyWithHttpInfo();

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        Task<IEnumerable<string>> BirdGetCompanyAsync();

        /// <summary>
        /// List All Companies of Birds
        /// </summary>
        /// <remarks>
        /// Return all birds companies defined by user&#39;s company.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        Task<ApiResponse<IEnumerable<string>>> BirdGetCompanyWithHttpInfoAsync();

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        IEnumerable<string> BirdGetCompanyName(string name);

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        ApiResponse<IEnumerable<string>> BirdGetCompanyNameWithHttpInfo(string name);

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        Task<IEnumerable<string>> BirdGetCompanyNameAsync(string name);

        /// <summary>
        /// List All Products of Bird company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird company name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        Task<ApiResponse<IEnumerable<string>>> BirdGetCompanyNameWithHttpInfoAsync(string name);
    }
}
