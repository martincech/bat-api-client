﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IFarmApi : IApiAccessor
    {
        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns> <see cref="Farm"/> </returns>
        Farm FarmGet(Guid farmId, string expand = null);

        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="Farm"/></returns>
        ApiResponse<Farm> FarmGetWithHttpInfo(Guid farmId, string expand = null);

        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of <see cref="Farm"/></returns>
        Task<Farm> FarmGetAsync(Guid farmId, string expand = null);

        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Farm"/>)</returns>
        Task<ApiResponse<Farm>> FarmGetWithHttpInfoAsync(Guid farmId, string expand = null);

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns><see cref="IEnumerable{Farm}"/></returns>
        IEnumerable<Farm> FarmGet(string expand = null);

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Farm}"/></returns>
        ApiResponse<IEnumerable<Farm>> FarmGetWithHttpInfo(string expand = null);

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Farm}"/></returns>
        Task<IEnumerable<Farm>> FarmGetAsync(string expand = null);

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Farm}"/>)</returns>
        Task<ApiResponse<IEnumerable<Farm>>> FarmGetWithHttpInfoAsync(string expand = null);

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Farm"/></returns>
        Farm FarmPost(FarmCreate body);

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Farm"/></returns>
        ApiResponse<Farm> FarmPostWithHttpInfo(FarmCreate body);

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Farm"/></returns>
        Task<Farm> FarmPostAsync(FarmCreate body);

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Farm"/>)</returns>
        Task<ApiResponse<Farm>> FarmPostWithHttpInfoAsync(FarmCreate body);

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Farm"/></returns>
        Farm FarmPut(Guid farmId, Farm body);

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Farm"/></returns>
        ApiResponse<Farm> FarmPutWithHttpInfo(Guid farmId, Farm body);

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Farm"/></returns>
        Task<Farm> FarmPutAsync(Guid farmId, Farm body);

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Farm"/>)</returns>
        Task<ApiResponse<Farm>> FarmPutWithHttpInfoAsync(Guid farmId, Farm body);

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        bool FarmDelete(Guid farmId);

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        ApiResponse<bool> FarmDeleteWithHttpInfo(Guid farmId);

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        Task<bool> FarmDeleteAsync(Guid farmId);

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        Task<ApiResponse<bool>> FarmDeleteWithHttpInfoAsync(Guid farmId);
    }
}
