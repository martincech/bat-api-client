﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    public interface IBirdTemplateApi : IBirdTemplateBaseApi
    {
        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns><see cref="IEnumerable{Bird}"/></returns>
        IEnumerable<Bird> BirdTemplateGetByProductName(string name);

        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Bird}"/></returns>
        ApiResponse<IEnumerable<Bird>> BirdTemplateGetByProductNameWithHttpInfo(string name);

        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns>Task of <see cref="IEnumerable{Bird}"/></returns>
        Task<IEnumerable<Bird>> BirdTemplateGetByProductNameAsync(string name);

        /// <summary>
        /// List All Bird Templates of product
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template product name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Bird}"/>)</returns>
        Task<ApiResponse<IEnumerable<Bird>>> BirdTemplateGetByProductNameWithHttpInfoAsync(string name);

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        IEnumerable<string> BirdTemplateGetCompany();

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        ApiResponse<IEnumerable<string>> BirdTemplateGetCompanyWithHttpInfo();

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        Task<IEnumerable<string>> BirdTemplateGetCompanyAsync();

        /// <summary>
        /// List All Companies of Bird Templates
        /// </summary>
        /// <remarks>
        /// Return all bird templates companies.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        Task<ApiResponse<IEnumerable<string>>> BirdTemplateGetCompanyWithHttpInfoAsync();

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns><see cref="IEnumerable{string}"/></returns>
        IEnumerable<string> BirdTemplateGetCompanyName(string name);

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{string}"/></returns>
        ApiResponse<IEnumerable<string>> BirdTemplateGetCompanyNameWithHttpInfo(string name);

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns>Task of <see cref="IEnumerable{string}"/></returns>
        Task<IEnumerable<string>> BirdTemplateGetCompanyNameAsync(string name);

        /// <summary>
        /// List All Products of Bird Template company
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="name">Bird template company name</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{string}"/>)</returns>
        Task<ApiResponse<IEnumerable<string>>> BirdTemplateGetCompanyNameWithHttpInfoAsync(string name);
    }
}
