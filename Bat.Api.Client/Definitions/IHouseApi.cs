﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public interface IHouseApi : IApiAccessor
    {
        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns><see cref="House"/></returns>
        House HouseGet(Guid houseId, string expand = null);

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="House"/></returns>
        ApiResponse<House> HouseGetWithHttpInfo(Guid houseId, string expand = null);

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of <see cref="House"/></returns>
        Task<House> HouseGetAsync(Guid houseId, string expand = null);

        /// <summary>
        /// Retrieve House Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="House"/>)</returns>
        Task<ApiResponse<House>> HouseGetWithHttpInfoAsync(Guid houseId, string expand = null);

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns><see cref="IEnumerable{House}"/></returns>
        IEnumerable<House> HouseGet(string expand = null);

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{House}"/></returns>
        ApiResponse<IEnumerable<House>> HouseGetWithHttpInfo(string expand = null);

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{House}"/></returns>
        Task<IEnumerable<House>> HouseGetAsync(string expand = null);

        /// <summary>
        /// List All Houses
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole House object. List of available parameters -  expand&#x3D;farm,scalesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{House}"/>)</returns>
        Task<ApiResponse<IEnumerable<House>>> HouseGetWithHttpInfoAsync(string expand = null);

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="House"/></returns>
        House HousePost(HouseCreate body);

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="House"/></returns>
        ApiResponse<House> HousePostWithHttpInfo(HouseCreate body);

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="House"/></returns>
        Task<House> HousePostAsync(HouseCreate body);

        /// <summary>
        /// Create New House
        /// </summary>
        /// <remarks>
        /// &#39;You may create a house.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="House"/>)</returns>
        Task<ApiResponse<House>> HousePostWithHttpInfoAsync(HouseCreate body);

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="House"/></returns>
        House HousePut(Guid houseId, House body);

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="House"/></returns>
        ApiResponse<House> HousePutWithHttpInfo(Guid houseId, House body);

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="House"/></returns>
        Task<House> HousePutAsync(Guid houseId, House body);

        /// <summary>
        /// Update House details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="House"/>)</returns>
        Task<ApiResponse<House>> HousePutWithHttpInfoAsync(Guid houseId, House body);

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        bool HouseDelete(Guid houseId);

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        ApiResponse<bool> HouseDeleteWithHttpInfo(Guid houseId);

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        Task<bool> HouseDeleteAsync(Guid houseId);

        /// <summary>
        /// Delete House
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="houseId">UUID v4 ID of House to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        Task<ApiResponse<bool>> HouseDeleteWithHttpInfoAsync(Guid houseId);
    }
}
