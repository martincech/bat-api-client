﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Models;

namespace Bat.Api.Client.Definitions
{
    public interface IUserBaseApi
    {
        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns><see cref="User"/></returns>
        User UserGet(Guid userId);

        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        ApiResponse<User> UserGetWithHttpInfo(Guid userId);

        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns>Task of <see cref="User"/></returns>
        Task<User> UserGetAsync(Guid userId);

        /// <summary>
        /// Retrieve User Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to return</param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        Task<ApiResponse<User>> UserGetWithHttpInfoAsync(Guid userId);

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns><see cref="IEnumerable{User}"/></returns>
        IEnumerable<User> UserGet();

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>ApiResponse of <see cref="IEnumerable{User}"/></returns>
        ApiResponse<IEnumerable<User>> UserGetWithHttpInfo();

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of <see cref="IEnumerable{User}"/></returns>
        Task<IEnumerable<User>> UserGetAsync();

        /// <summary>
        /// List All Users
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{User}"/>)</returns>
        Task<ApiResponse<IEnumerable<User>>> UserGetWithHttpInfoAsync();

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        User UserPost(UserCreate body);

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        ApiResponse<User> UserPostWithHttpInfo(UserCreate body);

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        Task<User> UserPostAsync(UserCreate body);

        /// <summary>
        /// Create New User
        /// </summary>
        /// <remarks>
        /// &#39;You may create a user.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        Task<ApiResponse<User>> UserPostWithHttpInfoAsync(UserCreate body);

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="User"/></returns>
        User UserPut(Guid userId, User body);

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="User"/></returns>
        ApiResponse<User> UserPutWithHttpInfo(Guid userId, User body);

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="User"/></returns>
        Task<User> UserPutAsync(Guid userId, User body);

        /// <summary>
        /// Update User details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="User"/>)</returns>
        Task<ApiResponse<User>> UserPutWithHttpInfoAsync(Guid userId, User body);

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        bool UserDelete(Guid userId);

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        ApiResponse<bool> UserDeleteWithHttpInfo(Guid userId);

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        Task<bool> UserDeleteAsync(Guid userId);

        /// <summary>
        /// Delete User
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="userId">UUID v4 ID of User to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        Task<ApiResponse<bool>> UserDeleteWithHttpInfoAsync(Guid userId);
    }
}
