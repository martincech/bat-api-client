﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class StatisticApi : CommonApi, IStatisticApi
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticApi"/> class.
        /// </summary>
        /// <returns></returns>
        public StatisticApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatisticApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public StatisticApi(Configuration configuration = null)
            : base(configuration)
        {
        }




        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        public IEnumerable<DailyStatistic> StatisticGetAllByDevice(Guid deviceId)
        {
            var response = StatisticGetAllByDeviceWithHttpInfo(deviceId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public ApiResponse<IEnumerable<DailyStatistic>> StatisticGetAllByDeviceWithHttpInfo(Guid deviceId)
        {
            return StatisticGetAllByDeviceWithHttpInfoAsync(deviceId).Result;
        }

        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public async Task<IEnumerable<DailyStatistic>> StatisticGetAllByDeviceAsync(Guid deviceId)
        {
            var response = await StatisticGetAllByDeviceWithHttpInfoAsync(deviceId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Device Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetAllByDeviceWithHttpInfoAsync(Guid deviceId)
        {
            // verify the required parameter 'deviceId' is set
            if (deviceId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(deviceId), nameof(StatisticApi), nameof(StatisticGetAllByDevice)));

            InitParameters();
            PathParameters.Add(nameof(deviceId), Configuration.ApiClient.ParameterToString(deviceId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/statistic/allByDevice/{deviceId}", Method.GET, nameof(StatisticGetAllByDevice));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<DailyStatistic>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<DailyStatistic>>(response.Data.ToString()));
        }

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        public IEnumerable<DailyStatistic> StatisticGetAllByFlock(Guid flockId)
        {
            var response = StatisticGetAllByFlockWithHttpInfo(flockId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public ApiResponse<IEnumerable<DailyStatistic>> StatisticGetAllByFlockWithHttpInfo(Guid flockId)
        {
            return StatisticGetAllByFlockWithHttpInfoAsync(flockId).Result;
        }

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public async Task<IEnumerable<DailyStatistic>> StatisticGetAllByFlockAsync(Guid flockId)
        {
            var response = await StatisticGetAllByFlockWithHttpInfoAsync(flockId);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Flock Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockId">UUID v4 ID of Flock to return</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetAllByFlockWithHttpInfoAsync(Guid flockId)
        {
            // verify the required parameter 'flockId' is set
            if (flockId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(flockId), nameof(StatisticApi), nameof(StatisticGetAllByFlock)));

            InitParameters();
            PathParameters.Add(nameof(flockId), Configuration.ApiClient.ParameterToString(flockId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/statistic/allByFlock/{flockId}", Method.GET, nameof(StatisticGetAllByFlock));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<DailyStatistic>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<DailyStatistic>>(response.Data.ToString()));
        }

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        public IEnumerable<DailyStatistic> StatisticGetLastByDevice(List<Guid> deviceIds)
        {
            var response = StatisticGetLastByDeviceWithHttpInfo(deviceIds);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public ApiResponse<IEnumerable<DailyStatistic>> StatisticGetLastByDeviceWithHttpInfo(List<Guid> deviceIds)
        {
            return StatisticGetLastByDeviceWithHttpInfoAsync(deviceIds).Result;
        }

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public async Task<IEnumerable<DailyStatistic>> StatisticGetLastByDeviceAsync(List<Guid> deviceIds)
        {
            var response = await StatisticGetLastByDeviceWithHttpInfoAsync(deviceIds);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Devices Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceIds"></param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetLastByDeviceWithHttpInfoAsync(List<Guid> deviceIds)
        {
            // verify the required parameter 'deviceIds' is set
            if (deviceIds == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(deviceIds), nameof(StatisticApi), nameof(StatisticGetLastByDevice)));

            InitParameters();
            var localVarPostBody = deviceIds.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(deviceIds) : deviceIds;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/statistic/lastByDevice", Method.GET, nameof(StatisticGetLastByDevice), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<DailyStatistic>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<DailyStatistic>>(response.Data.ToString()));
        }

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns><see cref="IEnumerable{DailyStatistic}"/></returns>
        public IEnumerable<DailyStatistic> StatisticGetLastByFlock(List<Guid> flockIds)
        {
            var response = StatisticGetLastByFlockWithHttpInfo(flockIds);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns>ApiResponse of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public ApiResponse<IEnumerable<DailyStatistic>> StatisticGetLastByFlockWithHttpInfo(List<Guid> flockIds)
        {
            return StatisticGetLastByFlockWithHttpInfoAsync(flockIds).Result;
        }

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns>Task of <see cref="IEnumerable{DailyStatistic}"/></returns>
        public async Task<IEnumerable<DailyStatistic>> StatisticGetLastByFlockAsync(List<Guid> flockIds)
        {
            var response = await StatisticGetLastByFlockWithHttpInfoAsync(flockIds);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Flocks Last Statistics Details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="flockIds"></param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{DailyStatistic}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<DailyStatistic>>> StatisticGetLastByFlockWithHttpInfoAsync(List<Guid> flockIds)
        {
            // verify the required parameter 'flockIds' is set
            if (flockIds == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(flockIds), nameof(StatisticApi), nameof(StatisticGetLastByFlock)));

            InitParameters();
            var localVarPostBody = flockIds.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(flockIds) : flockIds;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync("/statistic/lastByFlock", Method.GET, nameof(StatisticGetLastByFlock), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<DailyStatistic>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<DailyStatistic>>(response.Data.ToString()));
        }
    }
}
