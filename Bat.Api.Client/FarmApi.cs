﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class FarmApi : CommonApi, IFarmApi
    {
        private const string PathFarm = "/farm";
        private const string PathFarmById = "/farm/{farmId}";

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="FarmApi"/> class.
        /// </summary>
        /// <returns></returns>
        public FarmApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FarmApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public FarmApi(Configuration configuration = null)
            : base(configuration)
        {
        }

        #endregion


        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns> <see cref="Farm"/> </returns>
        public Farm FarmGet(Guid farmId, string expand = null)
        {
            var response = FarmGetWithHttpInfo(farmId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="Farm"/></returns>
        public ApiResponse<Farm> FarmGetWithHttpInfo(Guid farmId, string expand = null)
        {
            return FarmGetWithHttpInfoAsync(farmId, expand).Result;
        }

        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of <see cref="Farm"/></returns>
        public async Task<Farm> FarmGetAsync(Guid farmId, string expand = null)
        {
            var response = await FarmGetWithHttpInfoAsync(farmId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Farm Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Farm"/>)</returns>
        public async Task<ApiResponse<Farm>> FarmGetWithHttpInfoAsync(Guid farmId, string expand = null)
        {
            if (farmId == null) // verify the required parameter 'farmId' is set
            {
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(farmId), nameof(FarmApi), nameof(FarmGet)));
            }

            InitParameters();
            PathParameters.Add(nameof(farmId), Configuration.ApiClient.ParameterToString(farmId)); // path parameter
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFarmById, Method.GET, nameof(FarmGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Farm>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Farm>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns><see cref="IEnumerable{Farm}"/></returns>
        public IEnumerable<Farm> FarmGet(string expand = null)
        {
            var response = FarmGetWithHttpInfo(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Farm}"/></returns>
        public ApiResponse<IEnumerable<Farm>> FarmGetWithHttpInfo(string expand = null)
        {
            return FarmGetWithHttpInfoAsync(expand).Result;
        }

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Farm}"/></returns>
        public async Task<IEnumerable<Farm>> FarmGetAsync(string expand = null)
        {
            var response = await FarmGetWithHttpInfoAsync(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Farms
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Farm object. List of available parameters -  expand&#x3D;address,country,contactName,phone,email,activeFlocksCount, scheduledFlocksCount,housesCount,devicesCount (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Farm}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Farm>>> FarmGetWithHttpInfoAsync(string expand = null)
        {
            InitParameters();
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFarm, Method.GET, nameof(FarmGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Farm>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Farm>>(response.Data.ToString()));
        }

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Farm"/></returns>
        public Farm FarmPost(FarmCreate body)
        {
            var response = FarmPostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Farm"/></returns>
        public ApiResponse<Farm> FarmPostWithHttpInfo(FarmCreate body)
        {
            return FarmPostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Farm"/></returns>
        public async Task<Farm> FarmPostAsync(FarmCreate body)
        {
            var response = await FarmPostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Farm
        /// </summary>
        /// <remarks>
        /// &#39;You may create a farm.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Farm"/>)</returns>
        public async Task<ApiResponse<Farm>> FarmPostWithHttpInfoAsync(FarmCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
            {
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(FarmApi), nameof(FarmPost)));
            }

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFarm, Method.POST, nameof(FarmPost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Farm>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Farm>(response.Data.ToString()));
        }

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Farm"/></returns>
        public Farm FarmPut(Guid farmId, Farm body)
        {
            var response = FarmPutWithHttpInfo(farmId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Farm"/></returns>
        public ApiResponse<Farm> FarmPutWithHttpInfo(Guid farmId, Farm body)
        {
            return FarmPutWithHttpInfoAsync(farmId, body).Result;
        }

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Farm"/></returns>
        public async Task<Farm> FarmPutAsync(Guid farmId, Farm body)
        {
            var response = await FarmPutWithHttpInfoAsync(farmId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Farm details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Farm"/>)</returns>
        public async Task<ApiResponse<Farm>> FarmPutWithHttpInfoAsync(Guid farmId, Farm body)
        {
            // verify the required parameters are set
            if (farmId == null)
            {
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(farmId), nameof(FarmApi), nameof(FarmPut)));
            }
            if (body == null)
            {
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(FarmApi), nameof(FarmPut)));
            }

            InitParameters();
            PathParameters.Add(nameof(farmId), Configuration.ApiClient.ParameterToString(farmId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFarmById, Method.PUT, nameof(FarmPut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Farm>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Farm>(response.Data.ToString()));
        }

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        public bool FarmDelete(Guid farmId)
        {
            var response = FarmDeleteWithHttpInfo(farmId);
            return response.Data;
        }

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        public ApiResponse<bool> FarmDeleteWithHttpInfo(Guid farmId)
        {
            return FarmDeleteWithHttpInfoAsync(farmId).Result;
        }

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        public async Task<bool> FarmDeleteAsync(Guid farmId)
        {
            var response = await FarmDeleteWithHttpInfoAsync(farmId);
            return response.Data;
        }

        /// <summary>
        /// Delete Farm
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="farmId">UUID v4 ID of Farm to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        public async Task<ApiResponse<bool>> FarmDeleteWithHttpInfoAsync(Guid farmId)
        {
            // verify the required parameter 'farmId' is set
            if (farmId == null)
            {
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(farmId), nameof(FarmApi), nameof(FarmDelete)));
            }

            InitParameters();
            PathParameters.Add(nameof(farmId), Configuration.ApiClient.ParameterToString(farmId)); // path parameter
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathFarmById, Method.DELETE, nameof(FarmDelete));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }
    }
}
