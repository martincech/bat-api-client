﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Api.Common;
using Bat.Api.Client.Definitions;
using Bat.Api.Models;
using Newtonsoft.Json;
using RestSharp;

namespace Bat.Api.Client.Api
{
    /// <summary>
    /// Represents a collection of functions to interact with the API endpoints
    /// </summary>
    public partial class DeviceApi : CommonApi, IDeviceApi
    {
        private const string PathDevice = "/device";
        private const string PathDeviceById = "/device/{deviceId}";
        private const string PathDeviceSynchronize = "/device/sync/{deviceId}";


        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceApi"/> class.
        /// </summary>
        /// <returns></returns>
        public DeviceApi(string basePath)
            : base(basePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceApi"/> class
        /// using Configuration object
        /// </summary>
        /// <param name="configuration">An instance of Configuration</param>
        /// <returns></returns>
        public DeviceApi(Configuration configuration = null)
            : base(configuration)
        {
        }


        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns><see cref="Device"/></returns>
        public Device DeviceGet(Guid deviceId, string expand = null)
        {
            var response = DeviceGetWithHttpInfo(deviceId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="Device"/></returns>
        public ApiResponse<Device> DeviceGetWithHttpInfo(Guid deviceId, string expand = null)
        {
            return DeviceGetWithHttpInfoAsync(deviceId, expand).Result;
        }

        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="Device"/></returns>
        public async Task<Device> DeviceGetAsync(Guid deviceId, string expand = null)
        {
            var response = await DeviceGetWithHttpInfoAsync(deviceId, expand);
            return response.Data;
        }

        /// <summary>
        /// Retrieve Device Details
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to return</param>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="Device"/>)</returns>
        public async Task<ApiResponse<Device>> DeviceGetWithHttpInfoAsync(Guid deviceId, string expand = null)
        {
            // verify the required parameter 'deviceId' is set
            if (deviceId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(deviceId), nameof(DeviceApi), nameof(DeviceGet)));

            InitParameters();
            PathParameters.Add(nameof(deviceId), Configuration.ApiClient.ParameterToString(deviceId));
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathDeviceById, Method.GET, nameof(DeviceGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Device>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Device>(response.Data.ToString()));
        }

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns><see cref="IEnumerable{Device}"/></returns>
        public IEnumerable<Device> DeviceGet(string expand = null)
        {
            var response = DeviceGetWithHttpInfo(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>ApiResponse of <see cref="IEnumerable{Device}"/></returns>
        public ApiResponse<IEnumerable<Device>> DeviceGetWithHttpInfo(string expand = null)
        {
            return DeviceGetWithHttpInfoAsync(expand).Result;
        }

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of <see cref="IEnumerable{Device}"/></returns>
        public async Task<IEnumerable<Device>> DeviceGetAsync(string expand = null)
        {
            var response = await DeviceGetWithHttpInfoAsync(expand);
            return response.Data;
        }

        /// <summary>
        /// List All Devices
        /// </summary>
        /// <remarks>
        /// Return only basic structure of model compose of required properties and foreign keys.  If you want to get other model&#39;s properties see expand parameter.
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="expand">List of properties which you want to get in response separated by comma. E.g. expand&#x3D;all return whole Device object. List of available parameters -  expand&#x3D;description,farm,house,lastStatistics (optional)</param>
        /// <returns>Task of ApiResponse (<see cref="IEnumerable{Device}"/>)</returns>
        public async Task<ApiResponse<IEnumerable<Device>>> DeviceGetWithHttpInfoAsync(string expand = null)
        {
            InitParameters();
            AddExpandParameter(expand);
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathDevice, Method.GET, nameof(DeviceGet));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<IEnumerable<Device>>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<IEnumerable<Device>>(response.Data.ToString()));
        }

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns><see cref="Device"/></returns>
        public Device DevicePost(DeviceCreate body)
        {
            var response = DevicePostWithHttpInfo(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Device"/></returns>
        public ApiResponse<Device> DevicePostWithHttpInfo(DeviceCreate body)
        {
            return DevicePostWithHttpInfoAsync(body).Result;
        }

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Device"/></returns>
        public async Task<Device> DevicePostAsync(DeviceCreate body)
        {
            var response = await DevicePostWithHttpInfoAsync(body);
            return response.Data;
        }

        /// <summary>
        /// Create New Device
        /// </summary>
        /// <remarks>
        /// &#39;You may create a device.&#39;
        /// </remarks>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Device"/>)</returns>
        public async Task<ApiResponse<Device>> DevicePostWithHttpInfoAsync(DeviceCreate body)
        {
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(DeviceApi), nameof(DevicePost)));

            InitParameters();
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathDevice, Method.POST, nameof(DevicePost), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Device>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Device>(response.Data.ToString()));
        }

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="Device"/></returns>
        public Device DevicePut(Guid deviceId, Device body)
        {
            var response = DevicePutWithHttpInfo(deviceId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>ApiResponse of <see cref="Device"/></returns>
        public ApiResponse<Device> DevicePutWithHttpInfo(Guid deviceId, Device body)
        {
            return DevicePutWithHttpInfoAsync(deviceId, body).Result;
        }

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="Device"/></returns>
        public async Task<Device> DevicePutAsync(Guid deviceId, Device body)
        {
            var response = await DevicePutWithHttpInfoAsync(deviceId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Device details
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="Device"/>)</returns>
        public async Task<ApiResponse<Device>> DevicePutWithHttpInfoAsync(Guid deviceId, Device body)
        {
            // verify the required parameter 'deviceId' is set
            if (deviceId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(deviceId), nameof(DeviceApi), nameof(DevicePut)));
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(DeviceApi), nameof(DevicePut)));

            InitParameters();
            PathParameters.Add(nameof(deviceId), Configuration.ApiClient.ParameterToString(deviceId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathDeviceById, Method.PUT, nameof(DevicePut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<Device>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<Device>(response.Data.ToString()));
        }

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>Flag if successfully deleted</returns>
        public bool DeviceDelete(Guid deviceId)
        {
            var response = DeviceDeleteWithHttpInfo(deviceId);
            return response.Data;
        }

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>ApiResponse of Flag if successfully deleted</returns>
        public ApiResponse<bool> DeviceDeleteWithHttpInfo(Guid deviceId)
        {
            return DeviceDeleteWithHttpInfoAsync(deviceId).Result;
        }

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>Task of Flag if successfully deleted</returns>
        public async Task<bool> DeviceDeleteAsync(Guid deviceId)
        {
            var response = await DeviceDeleteWithHttpInfoAsync(deviceId);
            return response.Data;
        }

        /// <summary>
        /// Delete Device
        /// </summary>
        /// <exception cref="ApiException">Thrown when fails to make API call</exception>
        /// <param name="deviceId">UUID v4 ID of Device to Delete</param>
        /// <returns>Task of ApiResponse (Flag if successfully deleted)</returns>
        public async Task<ApiResponse<bool>> DeviceDeleteWithHttpInfoAsync(Guid deviceId)
        {
            // verify the required parameter 'deviceId' is set
            if (deviceId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(deviceId), nameof(DeviceApi), nameof(DeviceDelete)));

            InitParameters();
            PathParameters.Add(nameof(deviceId), Configuration.ApiClient.ParameterToString(deviceId));
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathDeviceById, Method.DELETE, nameof(DeviceDelete));
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<bool>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                response.Code == OkCode);
        }

        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns><see cref="DeviceSync"/></returns>
        public DeviceSync DeviceSynchronizePut(Guid deviceId, DeviceSync body)
        {
            var response = DeviceSynchronizePutWithHttpInfo(deviceId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="DeviceSync"/>)</returns>
        public ApiResponse<DeviceSync> DeviceSynchronizePutWithHttpInfo(Guid deviceId, DeviceSync body)
        {
            return DeviceSynchronizePutWithHttpInfoAsync(deviceId, body).Result;
        }

        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of <see cref="DeviceSync"/></returns>
        public async Task<DeviceSync> DeviceSynchronizePutAsync(Guid deviceId, DeviceSync body)
        {
            var response = await DeviceSynchronizePutWithHttpInfoAsync(deviceId, body);
            return response.Data;
        }

        /// <summary>
        /// Update Device from collector
        /// </summary>
        /// <param name="deviceId">UUID v4 ID of Device to Update</param>
        /// <param name="body"></param>
        /// <returns>Task of ApiResponse (<see cref="DeviceSync"/>)</returns>
        public async Task<ApiResponse<DeviceSync>> DeviceSynchronizePutWithHttpInfoAsync(Guid deviceId, DeviceSync body)
        {
            // verify the required parameter 'deviceId' is set
            if (deviceId == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(deviceId), nameof(DeviceApi), nameof(DeviceSynchronizePut)));
            // verify the required parameter 'body' is set
            if (body == null)
                throw new ApiException(BadRequestCode, string.Format(CultureInfo.CurrentCulture, global::Api.Common.Properties.Resources.MissingRequiredParameter, nameof(body), nameof(DeviceApi), nameof(DeviceSynchronizePut)));

            InitParameters();
            PathParameters.Add(nameof(deviceId), Configuration.ApiClient.ParameterToString(deviceId));
            var localVarPostBody = body.GetType() != typeof(byte[]) ? (object)ApiClient.Serialize(body) : body;
            AddOAuth2Authentication();

            var localVarResponse = await MakeRequestAsync(PathDeviceSynchronize, Method.PUT, nameof(DeviceSynchronizePut), localVarPostBody);
            var response = (ApiResponse)Configuration.ApiClient.Deserialize(localVarResponse, typeof(ApiResponse));

            return new ApiResponse<DeviceSync>((int)localVarResponse.StatusCode,
                response.Message,
                localVarResponse.Headers.ToDictionary(x => x.Name, x => x.Value.ToString()),
                JsonConvert.DeserializeObject<DeviceSync>(response.Data.ToString()));
        }
    }
}
